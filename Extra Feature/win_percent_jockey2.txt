CREATE OR REPLACE FUNCTION r_j_winrate (jockey text, rid text, total text)
RETURNS text AS $$
declare
	r_j_winrate text;
BEGIN
execute'
select
case 
    when 
    $3::float = 0 then 0 
    else
(win.win/$3::float) 
end AS r_j_winrate

from	
(select count(*)::float as win from records
 where rjockey = $1 
 and race_id::integer < $2::integer
 and r_place = ''1'' ) as win'
 
 into r_j_winrate USING jockey,rid,total;
   RETURN r_j_winrate;
END;
$$ LANGUAGE plpgsql;

alter table records
add column r_j_winrate text;

update records 
set r_j_winrate  = alias.r_j_winrate
from (select  "r_j_winrate"(records.rjockey, records.race_id, r_j_total) as r_j_winrate, race_id, rjockey,r_j_total from records 
) as alias 
where records.rjockey = alias.rjockey 
and records.race_id = alias.race_id
and records.r_j_total = alias.r_j_total;
