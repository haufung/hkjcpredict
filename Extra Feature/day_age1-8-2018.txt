CREATE OR REPLACE FUNCTION day_f_race (hor_id text, rid text )
RETURNS text AS $$
declare
	day_f_race integer;
BEGIN
   select (alias.r_date -  alias2.r_date) as day_f_race  from
(select r_date  from records,race 
 where horse_id = hor_id and records.race_id = race.race_id and records.race_id = rid) as alias,
(select r_date  from records,race 
 where horse_id = hor_id and records.race_id = race.race_id 
  order by records.race_id::integer asc limit 1 ) as alias2 
   into day_f_race;
   RETURN day_f_race;
END;
$$ LANGUAGE plpgsql;

########TEST#############
begin;
alter table records
add column day_age text;

update records 
set day_age = alias.day_f_race
from (select  "day_f_race"(records.horse_id, records.race_id) as day_f_race, horse_id, r_date, race.race_id from race, records 
where race.race_id = records.race_id) as alias
where records.race_id = alias.race_id and records.horse_id = alias.horse_id;

select * from records where day_age is null






