/*#############10 to power of function(avoid float over/underflow)################*/
CREATE OR REPLACE FUNCTION power10(no1 float, no2 float)
RETURNS float AS $$
declare
    result1 float;
BEGIN

select case 
when (no1 - no2)/400 < -309 then 0
when (no1 - no2)/400 > 308 then power(10,308)
else power(10,(no1 - no2)/400) end as result1

into result1;
   RETURN result1;
END;
$$ LANGUAGE plpgsql;
/*############Compute elo WITH KKKKKK############*/
CREATE OR REPLACE FUNCTION compute_elo_k(nk integer, rid text)
RETURNS void AS $$

BEGIN 
update records  
set before_j_elo = alias.past_elo
from (select "past_elo"(rjockey, race_id),race_id,rjockey from records where race_id = rid) as alias
where alias.race_id = records.race_id and alias.rjockey = records.rjockey;


update records 
set after_j_elo = alias3.before_j_elo::float + nk*(alias2.sf::float - alias1.esf::float)
from
(select "esf"(rjockey, race_id, before_j_elo) as esf,rjockey from records where  race_id = rid) as alias1,
(select "sf"(rjockey, race_id) as sf,rjockey from records where  race_id = rid) as alias2,
(select before_j_elo,rjockey from records where race_id = rid) as alias3
where records.rjockey = alias1.rjockey
and records.rjockey = alias2.rjockey
and records.rjockey = alias3.rjockey
and records.race_id = rid;

END;
$$ LANGUAGE plpgsql;
/*############Return 1500 if not find past elo############*/
DROP FUNCTION past_elo(text,text);
CREATE OR REPLACE FUNCTION past_elo(jockey text, rid text)
RETURNS text AS $$
declare
	past_elo text;
BEGIN

SELECT
coalesce(
(SELECT 
after_j_elo::float from records where rjockey = jockey
 and race_id::integer < rid::integer
 group by race_id,after_j_elo
 order by race_id::integer desc limit 1),1500)
   into past_elo;
   RETURN past_elo;
END;
$$ LANGUAGE plpgsql;
/*###########Funtion Estimate Score############*/
DROP FUNCTION esf(text,text,text);
CREATE OR REPLACE FUNCTION esf(jockey text, rid text , this_before_elo text)
RETURNS text AS $$
declare
    esf float;
	r_hr_num integer := (select count(race_id)::integer from records where race_id = rid group by race_id);
BEGIN

select alias.nom/((r_hr_num::float*(r_hr_num::float - 1))/2) as esf from (
select sum(1/(1+ "power10"(before_j_elo::float,this_before_elo::float)))::float as nom from records where 
race_id = rid and
rjockey != jockey
group by race_id) as alias

   into esf;
   RETURN esf;
   exception when numeric_value_out_of_range then 
        raise exception 'Value out of range for rjockey = % rid= % this_before_elo= % r_hr_num= %  esf= %', rjockey, rid, this_before_elo, r_hr_num, esf;
END;
$$ LANGUAGE plpgsql;

/*#########Score function############*/
DROP FUNCTION sf(text,text);
CREATE OR REPLACE FUNCTION sf(jockey text, rid text)
RETURNS text AS $$
declare
    sf double precision;
	nw_hr_num integer := (select coalesce((select count(race_id)/2  from records where r_place like '% DH' and race_id = rid group by race_id), 0))::integer;
	n integer := (select count(race_id)::integer from records where race_id = rid group by race_id) - nw_hr_num;
BEGIN


 select case when r_place like '% DH'
 then (n - (select coalesce(substring(r_place from '(..) DH'), substring(r_place from '(.) DH')))::float+0.5)/((n*(n-1))/2)
 else  (n - r_place::float )/((n*(n-1))/2)
 end as sf from records where race_id = rid and rjockey = jockey
   into sf;
   RETURN sf;

END;
$$ LANGUAGE plpgsql;
