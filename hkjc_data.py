import pandas as pd
import tensorflow as tf
import numpy as np

TRAIN_COLUMN_NAMES = ['r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age', 'h_day_apart',
                      'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate', 'before_h_elo',
                      'before_j_elo', 'before_t_elo', 'label']
TEST_COLUMN_NAMES = ['race_id', 'r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age',
                     'h_day_apart', 'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate',
                     'before_h_elo', 'before_j_elo', 'before_t_elo', 'label']

train_path = r"D:\Projects\hkjockey\data_for_tensorflow\used_data_10-9-2018\train_all.csv"
test_path = r"D:\Projects\hkjockey\data_for_tensorflow\used_data_10-9-2018\test.csv"
val_test_path = r"D:\Projects\hkjockey\data_for_tensorflow\used_data_10-9-2018\val_test.csv"


def label_to_class(series):
    series.loc[-1] = np.int64(1)
    series.loc[-2] = np.int64(0)
    series = pd.get_dummies(series)
    series = series.drop(series.index[[-1, -2]])

    return series


def load_data(my_feature_column):
    train = pd.read_csv(train_path, names=TRAIN_COLUMN_NAMES, header=0,
                        converters={'h_past_place': str,
                                    'distance': str,
                                    'age': str,
                                    'draw': str})
    # Shuffle all data and reset the row number
    train = train.sample(frac=1).reset_index(drop=True)
    train_x, train_y = train, train.pop('label')
    train_y = label_to_class(train_y).values
    train_x = tf.feature_column.input_layer(dict(train_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        train_x = sess.run(train_x)

    test = pd.read_csv(test_path, names=TEST_COLUMN_NAMES, header=0,
                       converters={'race_id': str,
                                   'h_past_place': str,
                                   'distance': str,
                                   'age': str,
                                   'draw': str})

    test_batch = test.groupby("race_id").size()
    _ = test.pop('race_id')
    test_x, test_y = test, test.pop('label')
    test_y = label_to_class(test_y).values
    test_x = tf.feature_column.input_layer(dict(test_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        test_x = sess.run(test_x)

    val_test = pd.read_csv(val_test_path, names=TEST_COLUMN_NAMES, header=0,
                           converters={'race_id': str,
                                       'h_past_place': str,
                                       'distance': str,
                                       'age': str,
                                       'draw': str})

    val_test_batch = val_test.groupby("race_id").size()
    _ = val_test.pop('race_id')
    val_test_x, val_test_y = val_test, val_test.pop('label')
    val_test_y = label_to_class(val_test_y).values
    val_test_x = tf.feature_column.input_layer(dict(val_test_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        val_test_x = sess.run(val_test_x)

    return (train_x, train_y), (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch)