import pandas as pd
import tensorflow as tf


# with open(r'‪D:\Projects\hkjockey\data_not_norm.csv'.replace('\u202a',""), 'r') as csvfile:
def predict_sam(sample, i):
    with open(sample.replace('\u202a', ""), 'r') as csvfile:
        df = pd.read_csv(csvfile, nrows=20)
        this_dict = df.loc[[i]].to_dict('list')
        this_dict['distance'][0] = str(this_dict['distance'][0])
        this_dict['age'][0] = str(this_dict['age'][0])
        this_dict['draw'][0] = str(this_dict['draw'][0])
        this_dict['h_past_place'][0] = str(this_dict['h_past_place'][0])
        del this_dict['label']

    return this_dict


def get_feature_column():
    r_loc = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'r_loc', ['ST', 'HV']))
    distance = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'distance', ['1600', '2400', '1200', '2000', '1650', '2200', '1400', '1800', '1000']))
    going = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'going', ["YIELDING", "WET SLOW", "YIELDING TO SOFT", "GOOD TO FIRM", "SLOW",
                  "GOOD", "WET FAST", "GOOD TO YIELDING", "SOFT", "FAST"]))
    course = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'course', ["TURF - B COURSE - Worked Back", "TURF - B+2 COURSE", "TURF - C COURSE - Worked Back",
                   "TURF - A COURSE", "TURF - A+3 COURSE", "TURF - B COURSE", "TURF - C COURSE",
                   "TURF - C+3 COURSE - Worked Back", "TURF - C+3 COURSE", "ALL WEATHER TRACK"]))
    act_wt = tf.feature_column.numeric_column(
        'act_wt')
    decalr_wt = tf.feature_column.numeric_column(
        'decalr_wt')
    draw = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'draw', ["1", "4", "9", "8", "12", "10", "7", "13", "15", "5", "14", "6", "3", "11", "2"]))
    age = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'age', ["9", "4", "8", "6", "3", "10", "11", "2", "7", "5"]))
    h_day_apart = tf.feature_column.numeric_column(
        'h_day_apart')
    h_past_place = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'h_past_place', ["11 DH", "3 DH", "8", "NULL", "12", "10 DH", "10", "WX", "7", "FE", "13", "1 DH",
                         "5", "14", "DISQ", "6", "WX-A", "2", "2 DH", "9 DH", "UR", "WV", "4 DH", "1", "DNF", "4", "9",
                         "5 DH",
                         "6 DH", "7 DH", "3", "11", "8 DH", "12 DH", "WV-A", "PU", "TNP"]))
    r_wgt_diff = tf.feature_column.numeric_column(
        'r_wgt_diff')
    r_h_winrate = tf.feature_column.numeric_column(
        'r_h_winrate')
    r_j_winrate = tf.feature_column.numeric_column(
        'r_j_winrate')
    r_t_winrate = tf.feature_column.numeric_column(
        'r_t_winrate')
    before_h_elo = tf.feature_column.numeric_column(
        'before_h_elo')
    before_j_elo = tf.feature_column.numeric_column(
        'before_j_elo')
    before_t_elo = tf.feature_column.numeric_column(
        'before_t_elo')
    r_loc_x_draw_x_course = tf.feature_column.crossed_column(
        [tf.feature_column.categorical_column_with_vocabulary_list(
            'r_loc', ['ST', 'HV', 'CH'])
            , tf.feature_column.categorical_column_with_vocabulary_list(
            'draw', ["1", "4", "9", "8", "12", "10", "7", "13", "15", "5", "14", "6", "3", "11", "2"]),
            tf.feature_column.categorical_column_with_vocabulary_list(
                'course', ["TURF - B COURSE - Worked Back", "TURF - B+2 COURSE", "TURF - C COURSE - Worked Back",
                           "TURF - A COURSE", "TURF - A+3 COURSE", "TURF - B COURSE", "TURF - C COURSE",
                           "TURF - C+3 COURSE - Worked Back", "TURF - C+3 COURSE", "ALL WEATHER TRACK"])], 50000)
    r_loc_x_draw_x_course_embedded = tf.feature_column.embedding_column(r_loc_x_draw_x_course, 50)
    my_feature_column = [r_loc, distance, going, course, act_wt, decalr_wt
        , draw, age, h_day_apart, h_past_place, r_wgt_diff, r_h_winrate
        , r_j_winrate, r_t_winrate, before_h_elo, before_j_elo
        , before_t_elo, r_loc_x_draw_x_course_embedded]

    return my_feature_column
