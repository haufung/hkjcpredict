import tensorflow as tf
import numpy as np
import time
import sys

sys.path.append(r"D:\Projects\hkjockey\py\tensorflow19-9-2018_adj_lr_pred")
import hkjc_data
import hkjc_feature_column

export_path = r"D:\Projects\hkjockey\py\tensorflow19-9-2018_adj_lr_pred\model"
example_len = ['r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age', 'h_day_apart',
               'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate', 'before_h_elo',
               'before_j_elo', 'before_t_elo']

my_feature_column = hkjc_feature_column.get_feature_column()
print("Loading data...")
(train_x, train_y), (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = hkjc_data.load_data(
    my_feature_column)
print("Completed!")
n_nodes_hl1 = 100
n_nodes_hl2 = 50

n_classes = 2
hm_epochs = 10
batch_size = 100

x = tf.placeholder('float', shape=[None, 153])
y = tf.placeholder('float', shape=[None, 2])


# Nothing changes
def neural_network_model(net):
    for units in [100, 50]:
        net = tf.layers.dense(net, units=units, activation=tf.nn.relu)
    logits = tf.layers.dense(net, units=n_classes, activation=None)

    return logits


def train_neural_network(x):
    prediction = neural_network_model(x)
    pred_summary = tf.summary.histogram("prediction", prediction)

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=y))
    merged = tf.summary.merge([pred_summary])

    optimizer = tf.train.AdamOptimizer(learning_rate=0.00075).minimize(loss)

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    with tf.Session() as sess:
        train_writer = tf.summary.FileWriter(r"D:\Projects\hkjockey\py\tensorflow11-9-2018_adj_lr_pred\train",
                                             sess.graph)
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        duration1 = 0
        for epoch in range(hm_epochs):
            time_start = time.time()
            epoch_loss = 0
            i = 0
            while i < len(train_x):
                start = i
                end = i + batch_size
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
                summary, _, c = sess.run([merged, optimizer, loss], feed_dict={x: batch_x,
                                                                               y: batch_y})
                train_writer.add_summary(summary, (epoch + 1))
                epoch_loss += c
                i += batch_size

            epoch_summary = tf.Summary(value=[tf.Summary.Value(tag="epoch_loss", simple_value=epoch_loss)])
            train_writer.add_summary(epoch_summary, (epoch + 1))

            time_stop = time.time()
            duration2 = time_stop - time_start
            duration1 += duration2

            if (epoch + 1) % 10 == 0:
                # correct = tf.equal(tf.argmax(tf.nn.softmax(prediction,0), 0)[1], tf.argmax(y, 0)[1])
                correct = tf.equal(tf.argmax(tf.nn.softmax(prediction, 1), 0)[1], tf.argmax(y, 0)[1])
                win = tf.reduce_sum(tf.cast(correct, 'float'))
                num_race = len(val_test_batch)
                val_total_win = 0
                i = 0

                # Val_win
                for batch_size2 in val_test_batch:
                    start = i
                    end = i + batch_size2
                    val_batch_x = np.array(val_test_x[start:end])
                    val_batch_y = np.array(val_test_y[start:end])
                    i += batch_size2
                    val_this_win = sess.run(win, feed_dict={x: val_batch_x,
                                                            y: val_batch_y})
                    val_total_win += val_this_win

                val_acc = val_total_win / num_race
                print('Val_win:', val_total_win, 'Out of:', num_race, 'Val_Acc: %s' % float('%.4g' % val_acc))

                num_race = len(test_batch)
                total_win = 0
                i = 0

                # Test_Win
                for batch_size1 in test_batch:
                    start = i
                    end = i + batch_size1
                    batch_x = np.array(test_x[start:end])
                    batch_y = np.array(test_y[start:end])
                    i += batch_size1
                    this_win = sess.run(win, feed_dict={x: batch_x,
                                                        y: batch_y})
                    total_win += this_win

                test_acc = total_win / num_race
                print('Test_win:', total_win, 'Out of:', num_race, 'Acc: %s' % float('%.4g' % test_acc))
                print('Epoch', epoch + 1, 'completed out of', hm_epochs, 'loss:', epoch_loss, 'duration:', duration1)
                duration1 = 0
                test_summary = tf.Summary(value=[tf.Summary.Value(tag="test_acc", simple_value=test_acc),
                                                 tf.Summary.Value(tag="val_acc", simple_value=val_acc)])
                train_writer.add_summary(test_summary, (epoch + 1))
                save_path = saver.save(sess, export_path + "\model.ckpt" + str((epoch + 1)))
                # tf.saved_model.simple_save(sess,
                # export_path,
                # inputs={"x": x, "y": y},
                # outputs={"prediction": prediction})


train_neural_network(x)
