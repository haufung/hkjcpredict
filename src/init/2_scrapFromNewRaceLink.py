import datetime
import re
import time

from bs4 import BeautifulSoup

from src.config.horseRaceDataDbConnection import connection
from src.utils import seleniumUtil, webDriverUtil
from selenium import webdriver

cur = connection.cursor()
NO_OF_PAGE_TO_LOAD_CSS_SELECTOR = "body > div > div.f_clear.top_races > table > tbody > tr:nth-child(1) > td:not([class]) img"
LOCATION_AND_DATE_CSS_SELECTOR = "body > div > div.raceMeeting_select > p:nth-child(1) > span.f_fl.f_fs13"
RACE_NUMBER_AND_RACE_ID_CSS_SELECTOR = "body > div > div.race_tab > table > thead > tr > td:nth-child(1)"
CLS_GRP_AND_DISTANCE_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(1)"
RACE_NAME_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(1)"
GOING_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(3)"
COURSE_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(3)"
POOL_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(4) > td:nth-child(1)"
RECORDS_TABLE_CSS_SELECTOR = "body > div > div.performance > table > tbody > tr"

recordsTable = 'init.init_records'
raceTable = 'init.init_race'
allRaceUrlTable = 'init.all_race_url'
options, chrome_driver_path = webDriverUtil.webDriverSetup()
driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)

def queryAllRaceUrl():
    cur.execute(f'''select * from init.all_race_day where race_date <= '2010-05-30' order by race_date desc''')
    return cur.fetchall()

def rmspace(str):
    tmp = []
    for i in range(len(str)):
        try:
            if str[i] == ' ' and str[i + 1] == ' ':
                break
            else:
                tmp.append(str[i])
        except IndexError:
            break

    word = "".join(tmp)
    return word


def updateDebugLog(scrapUrl, errorMessage, reference, generalReference):
    cur.execute(
        f'''INSERT INTO init.debug_log (url, error_message, created_date, reference, general_reference) VALUES (%s,%s, NOW(), %s, %s);''',
        [scrapUrl, errorMessage, reference, generalReference])
    connection.commit()


def scrapLocation(sou, scrapUrl):
    locationString = sou.select(LOCATION_AND_DATE_CSS_SELECTOR)[0].text
    loc = ""
    if "Sha Tin" in locationString:
        loc = "ST"
    elif "Happy Valley" in locationString:
        loc = "HV"
    elif "Conghua" in locationString:
        loc = "CH"
    else:
        updateDebugLog(scrapUrl, "ERROR IN RACE LOCATION", "", "")
    return loc


def generateAllRaceUrl(url, loc, num):
    link = []
    for j in range(num):
        tmp = url + "/" + loc + "/" + str(j + 1)
        link.append(tmp)
    return link


def scrapRaceDate(sou):
    locationAndDateString = sou.select(LOCATION_AND_DATE_CSS_SELECTOR)[0].text
    matchDate = re.search('\d{2}/\d{2}/\d{4}', locationAndDateString)
    date = datetime.datetime.strptime(matchDate.group(), '%d/%m/%Y').date()
    return date


def scrapRaceNumber(sou):
    stringList = sou.select(RACE_NUMBER_AND_RACE_ID_CSS_SELECTOR)[0].text
    stringToReturn = ""
    for k in range(len(stringList)):
        if stringList[k] == " " and stringList[k + 2] == " ":
            stringToReturn = int(stringList[k + 1])
        elif stringList[k] == " " and stringList[k + 3] == " ":
            stringToReturn = int(stringList[k + 1]) * 10 + int(stringList[k + 2])
    return stringToReturn


def validateRaceNumberAndRaceDate(url, dateA, dateB, raceNumA, raceNumB):
    if dateA != dateB:
        updateDebugLog(url, "DATE SCRAPED IS UNEXPECTED", "_".join([dateA, dateB]), "")
    if raceNumA != raceNumB:
        updateDebugLog(url, "RACE NUM SCRAPED IS UNEXPECTED", "_".join([raceNumA, raceNumB]), "")


def generateUniqueRaceId(date, raceNum):
    dateString = date.strftime("%Y%m%d")
    listToBeJoined = [dateString]
    if len(str(scrapedRaceNumber)) == 1:  # raceNumber 0 - 9
        listToBeJoined.append("0" + str(raceNum))
    elif len(str(raceNum)) > 1:  # raceNumber > 9
        listToBeJoined.append(str(raceNum))
    return "_".join(listToBeJoined)


def scrapClassDistanceRating(raceId, url, sou):
    soupString = sou.select(CLS_GRP_AND_DISTANCE_CSS_SELECTOR)[0].contents[0]
    stringList = soupString.split(" - ")
    if len(stringList) == 1:
        updateDebugLog(url, "SOMETHING WRONG WITH CLASS / DISTANCE / RATING", soupString, raceId)
        return "", "", ""
    elif len(stringList) == 1:
        return stringList[0], "", ""
    elif len(stringList) == 2:
        return stringList[0], stringList[1], ""
    elif len(stringList) == 3:
        return stringList[0], stringList[1], stringList[2]
    elif len(stringList) > 3:
        updateDebugLog(url, "SOMETHING WRONG WITH CLASS / DISTANCE / RATING", soupString, raceId)
        return stringList[0], stringList[1], stringList[2]


def insertIntoInitRecordsTable(race_id, place, horse_number, horse_id, jockey_name, trainer_name, actual_weight,
                               declar_weight, draw, lbw, running_position, finish_time, win_odds, records_date):
    cur.execute(rf'''INSERT INTO {recordsTable}
("records_id",
"race_id",
"place",
"horse_number",
"horse_id",
"jockey_name",
"trainer_name",
"actual_weight",
"declare_weight",
"draw",
"lbw",
"running_position",
"finish_time",
"win_odds",
"records_date")
VALUES
(%s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s)''',
                [race_id + "_" + horse_id, race_id, place, horse_number, horse_id, jockey_name, trainer_name,
                 actual_weight, declar_weight, draw, lbw, running_position, finish_time, win_odds, records_date])


def insertIntoInitRaceTable(race_id, race_date, location, race_number, race_class, distance, going, course, pool,
                            race_name, rating):
    cur.execute(rf'''INSERT INTO {raceTable}
    ("race_id",
"race_date",
"location",
"race_number",
"class",
"distance",
"going",
"course",
"pool",
"race_name",
"rating")
    VALUES
    (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                [race_id, race_date, location, race_number, race_class, distance, going, course, pool, race_name,
                 rating])


def insertIntoInitAllRaceUrlTable(raceId, url):
    cur.execute(rf'''INSERT INTO {allRaceUrlTable}
        ("race_id",
        "url")
        VALUES
        ('{raceId}','{url}')''')


def scrapRecordsTable(table, num):
    place = []
    horseId = []
    jockey = []
    trainer = []
    actualWeight = []
    declareWeight = []
    draw = []
    lbw = []
    runningPosition = []
    finishTime = []
    winOdds = []
    for i in range(num):
        place.append(table[i].contents[1].text)

        tmpHorseId = table[i].contents[5].text
        tmpHorseId2 = re.findall(r"\(([A-Za-z0-9_]+)\)", tmpHorseId)[0]
        horseId.append(tmpHorseId2)

        tmpJockey = table[i].contents[7].text
        tmpJockey2 = re.findall(r"\w+|'|-", tmpJockey)
        jockey.append(" ".join(tmpJockey2))

        tmpTrainer = table[i].contents[9].text
        tmpTrainer2 = re.findall(r"\w+|'|-", tmpTrainer)
        trainer.append(" ".join(tmpTrainer2))

        actualWeight.append(table[i].contents[11].text)
        declareWeight.append(table[i].contents[13].text)
        draw.append(table[i].contents[15].text)
        lbw.append(table[i].contents[17].text)

        finishTime.append(table[i].contents[21].text)

        tmp = table[i].contents[19].text
        tmp = re.findall(r"\d+", tmp)
        if not tmp:
            runningPosition.append("-")
        else:
            runningPosition.append(" ".join(tmp))

        if len(table[i].contents) < 24:
            # for those with no win odds
            winOdds.append("")
        else:
            winOdds.append(table[i].contents[23].text)

    return place, horseId, jockey, trainer, actualWeight, declareWeight, draw, lbw, runningPosition, finishTime, winOdds

def checkSoupHaveRefund(sou, dri):
    if "REFUND" in sou.get_text():
        updateDebugLog(dri.current_url, "RACE HAVE REFUNDED", sou.get_text(), "")
        return True
    else:
        return False

def checkSoupHaveNoInformation(sou):
    if "No information." in sou.get_text():
        return True
    else:
        return False

def checkSoupHaveCookieChecking(sou):
    if "Cookies must be enabled in order to view this page." in sou.get_text():
        return True
    else:
        return False

def wait5SecondAndRefresh(url, dri):
    time.sleep(5)
    dri.refresh()
    seleniumUtil.checkLoading(dri)
    dri.get(url)
    seleniumUtil.checkLoading(dri)


def refreshForNoInformation(url, dri):
    wait5SecondAndRefresh(url, dri)
    return checkSoupHaveNoInformation(BeautifulSoup(dri.page_source, 'html.parser'))

def refreshForCookieChecking(url, dri):
    wait5SecondAndRefresh(url, dri)
    return checkSoupHaveCookieChecking(BeautifulSoup(dri.page_source, 'html.parser'))

def driverGetCatchException(url, dri):
    startTime = time.time()
    try:
        dri.get(url)
    except Exception as e:
        errorTime = time.time()
        updateDebugLog(url, str(e), str(int(errorTime-startTime)), "")
        dri = seleniumUtil.rotateUserAgent(dri)
        dri.get(url)
    return dri



allRaceUrl = queryAllRaceUrl()

for raceTuple in allRaceUrl:
    # url ="http://racing.hkjc.com/racing/info/meeting/Results/English/Local/20171122/HV"
    # url = "http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171001/ST/7"
    dateFromDB = raceTuple[0]
    firstRaceUrl = raceTuple[1]
    driver = driverGetCatchException(firstRaceUrl, driver)
    seleniumUtil.checkLoading(driver)

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    if checkSoupHaveNoInformation(soup):
        if refreshForNoInformation(firstRaceUrl, driver):
            updateDebugLog(driver.current_url, "RACE HAVE NO INFORMATION", soup.get_text(), "")
            continue
        else:
            soup = BeautifulSoup(driver.page_source, 'html.parser')

    if checkSoupHaveCookieChecking(soup):
        if refreshForCookieChecking(firstRaceUrl, driver):
            updateDebugLog(driver.current_url, "RACE HAVE COOKIE CHECKING", soup.get_text(), "")
            continue
        else:
            soup = BeautifulSoup(driver.page_source, 'html.parser')

    soup2 = soup.select(NO_OF_PAGE_TO_LOAD_CSS_SELECTOR)
    numberOfRace = len(soup2) - 1

    location = scrapLocation(soup, driver.current_url)
    raceUrls = generateAllRaceUrl(firstRaceUrl, location, numberOfRace)
    # Scrap through url in link
    for raceNumberFrom0 in range(len(raceUrls)):
        thisRaceUrl = raceUrls[raceNumberFrom0]
        driver = driverGetCatchException(thisRaceUrl, driver)
        seleniumUtil.checkLoading(driver)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        if checkSoupHaveNoInformation(soup):
            if refreshForNoInformation(thisRaceUrl, driver):
                updateDebugLog(driver.current_url, "RACE HAVE NO INFORMATION", soup.get_text(), "")
                continue
            else:
                soup = BeautifulSoup(driver.page_source, 'html.parser')

        if checkSoupHaveCookieChecking(soup):
            if refreshForCookieChecking(thisRaceUrl, driver):
                updateDebugLog(driver.current_url, "RACE HAVE COOKIE CHECKING", soup.get_text(), "")
                continue
            else:
                soup = BeautifulSoup(driver.page_source, 'html.parser')

        if checkSoupHaveRefund(soup, driver):
            continue

        scrapedRaceDate = scrapRaceDate(soup)
        scrapedRaceNumber = scrapRaceNumber(soup)

        validateRaceNumberAndRaceDate(driver.current_url, dateFromDB, scrapedRaceDate, raceNumberFrom0 + 1,
                                      scrapedRaceNumber)

        raceUniqueId = generateUniqueRaceId(scrapedRaceDate, scrapedRaceNumber)

        # r_class//Race
        # Group Three http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171001/ST/7
        # Group Two http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171022/ST/7
        # Group One http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20180225/ST/8

        raceClass, distance, rating = scrapClassDistanceRating(raceUniqueId, driver.current_url, soup)
        raceName = soup.select(RACE_NAME_CSS_SELECTOR)[0].text
        going = soup.select(GOING_CSS_SELECTOR)[0].text
        course = soup.select(COURSE_CSS_SELECTOR)[0].text
        pool = soup.select(POOL_CSS_SELECTOR)[0].text

        recordsTableSoup = soup.select(RECORDS_TABLE_CSS_SELECTOR)
        numberOfHorse = len(recordsTableSoup)
        place, horseId, jockey, trainer, actualWeight, declareWeight, \
        draw, lbw, runningPosition, finishTime, winOdds = scrapRecordsTable(
            recordsTableSoup, numberOfHorse)
        for i in range(numberOfHorse):
            insertIntoInitRecordsTable(raceUniqueId, place[i], numberOfHorse, horseId[i], jockey[i], trainer[i],
                                       actualWeight[i], declareWeight[i], draw[i], lbw[i], runningPosition[i],
                                       finishTime[i], winOdds[i], scrapedRaceDate)

        insertIntoInitRaceTable(raceUniqueId, scrapedRaceDate, location, scrapedRaceNumber,
                                raceClass, distance, going, course, pool, raceName, rating)
        insertIntoInitAllRaceUrlTable(raceUniqueId, driver.current_url)
        connection.commit()
driver.quit()
# http://www.hkjc.com/english/racing/results.asp?racedate=27/06/2007
# http://racing.hkjc.com/racing/info/meeting/Results/English/Local/20061223
