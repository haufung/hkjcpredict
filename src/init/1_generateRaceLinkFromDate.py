import os

from src.config.horseRaceDataDbConnection import connection
from src.utils import generatedFileUtil

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
fileName = generatedFileUtil.generateFileNameWithCurrentDate("newRaceLink.txt")
newRaceLinkFiles = DIR_PATH + rf"\..\generatedFiles\1_generateNewRaceLink{fileName}"
SCHEMA = 'init'
cur = connection.cursor()

cur.execute(f'select race_date from {SCHEMA}.all_race order by race_date asc')

for date in cur.fetchall():
    url = f'http://racing.hkjc.com/racing/info/meeting/Results/English/Local/{date[0].strftime("%Y%m%d")}'
    cur.execute(f"UPDATE init.all_race SET url = '{url}' where race_date = '{date[0]}'")
    print(url)

connection.commit()