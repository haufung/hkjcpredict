import os
import time

import numpy as np
import tensorflow as tf

# sys.path.append(r"D:\Projects\hkjockey\py\tensorflow19-9-2018_adj_lr_pred")
from src.utils import tensorflowDataPreProcess
from src.utils import tensorflowFeatureColumn

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
trainTestValDataSetPath = DIR_PATH + r"\generatedFiles\7_outputTrainTestValDataFromDB"

export_path = DIR_PATH + r"\generatedFiles\8_trainModel"
example_len = ['r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age', 'h_day_apart',
               'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate', 'before_h_elo',
               'before_j_elo', 'before_t_elo']

my_feature_column = tensorflowFeatureColumn.get_feature_column(None)
print("Loading data...")
(train_x, train_y, batchSizeMapping, trainGroupList), (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = tensorflowDataPreProcess.load_data(trainTestValDataSetPath,
                                                                                 my_feature_column)
print("Completed!")

n_classes = 2
hm_epochs = 200

x = tf.placeholder('float', shape=[None, 161], name="x")
y = tf.placeholder('float', shape=[None, 2], name="y")


def neural_network_model(net):
    net = tf.layers.dense(net, units=100, activation=tf.nn.relu)
    logits = tf.layers.dense(net, units=n_classes, activation=None)
    return logits


def testingWithBatch(sess, originalPrediction, pred, correct, win, batches, batch_x, batch_y):
    num_race = len(batches)
    total_win = 0
    i = 0

    for this_batch in batches:
        start = i
        end = i + this_batch
        this_batch_x = np.array(batch_x[start:end])
        this_batch_y = np.array(batch_y[start:end])
        i += this_batch
        this_originalPrediction, this_pred, this_correct, this_win = sess.run([originalPrediction, pred, correct, win],
                                                                              feed_dict={x: this_batch_x,
                                                                                         y: this_batch_y})
        total_win += this_win

    accuracy = total_win / num_race
    return total_win, num_race, accuracy


def train_neural_network(x):
    prediction = neural_network_model(x)
    pred_summary = tf.summary.histogram("prediction", prediction)
    pred0 = tf.nn.softmax(prediction, 0, name="pred0")
    pred1 = tf.nn.softmax(prediction, 1, name="pred1")

    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=y))
    merged = tf.summary.merge([pred_summary])

    optimizer = tf.train.AdamOptimizer(learning_rate=0.00075).minimize(loss)

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver(max_to_keep=50)

    with tf.Session() as sess:
        train_writer = tf.summary.FileWriter(export_path,
                                             sess.graph)
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        duration1 = 0
        for epoch in range(hm_epochs):
            time_start = time.time()
            epoch_loss = 0
            i = 0
            for _ in batchSizeMapping:
                this_race_id = trainGroupList[i][0]
                thisBatchSize = batchSizeMapping[this_race_id]
                start = i
                end = i + thisBatchSize
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
                summary, _, c, this_prediction = sess.run([merged, optimizer, loss, prediction], feed_dict={x: batch_x, y: batch_y})
                train_writer.add_summary(summary, (epoch + 1))
                epoch_loss += c
                i += thisBatchSize

            epoch_summary = tf.Summary(value=[tf.Summary.Value(tag="epoch_loss", simple_value=epoch_loss)])
            train_writer.add_summary(epoch_summary, (epoch + 1))

            time_stop = time.time()
            duration2 = time_stop - time_start
            duration1 += duration2

            if (epoch + 1) % 5 == 0:
                correct0 = tf.equal(tf.argmax(pred0, 0)[1], tf.argmax(y, 0)[1])
                win0 = tf.reduce_sum(tf.cast(correct0, 'float'), name="win0")

                (val_total_win, val_num_race, val_acc0) = testingWithBatch(sess, prediction, pred0, correct0, win0,
                                                                           val_test_batch, val_test_x, val_test_y)
                print('Val_win0:', val_total_win, 'Out of:', val_num_race, 'Val_Acc: %s' % float('%.4g' % val_acc0))

                (test_total_win, test_num_race, test_acc0) = testingWithBatch(sess, prediction, pred0, correct0, win0,
                                                                              test_batch, test_x, test_y)
                print('Test_win0:', test_total_win, 'Out of:', test_num_race, 'Acc: %s' % float('%.4g' % test_acc0))

                correct1 = tf.equal(tf.argmax(pred1, 0)[1], tf.argmax(y, 0)[1])
                win1 = tf.reduce_sum(tf.cast(correct1, 'float'), name="win1")

                (val_total_win, val_num_race, val_acc1) = testingWithBatch(sess, prediction, pred1, correct1, win1,
                                                                           val_test_batch, val_test_x, val_test_y)
                print('Val_win1:', val_total_win, 'Out of:', val_num_race, 'Val_Acc: %s' % float('%.4g' % val_acc1))

                (test_total_win, test_num_race, test_acc1) = testingWithBatch(sess, prediction, pred1, correct1, win1,
                                                                              test_batch, test_x, test_y)
                print('Test_win1:', test_total_win, 'Out of:', test_num_race, 'Acc: %s' % float('%.4g' % test_acc1))
                print('Epoch', epoch + 1, 'completed out of', hm_epochs, 'loss:', epoch_loss, 'duration:', duration1)

                duration1 = 0
                if (epoch + 1) % 5 == 0:
                    save_path = saver.save(sess, export_path + rf'\epoch{str((epoch + 1))}.ckpt')
                test_summary = tf.Summary(value=[tf.Summary.Value(tag="test_acc0", simple_value=test_acc0),
                                                 tf.Summary.Value(tag="val_acc0", simple_value=val_acc0),
                                                 tf.Summary.Value(tag="test_acc1", simple_value=test_acc1),
                                                 tf.Summary.Value(tag="val_acc1", simple_value=val_acc1)])
                train_writer.add_summary(test_summary, (epoch + 1))


train_neural_network(x)
