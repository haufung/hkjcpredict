import csv
import os
import sys
from src.config.horseRaceDataDbConnection import connection
from src.utils import generatedFileUtil, calculatedFieldUtil

cur = connection.cursor()

root_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(root_path)
calculatedFieldPath = root_path + r"\generatedFiles\4_calculatedField"

# select the highest calculated race_id
cur.execute(r'''select max(race_id::integer) from records where r_t_winrate is not null;''')
rows = cur.fetchall()
date = rows[0][0]

sql_path = root_path + r"\config\calculatedFieldSql"
del_path = calculatedFieldPath + generatedFileUtil.generateFileNameWithCurrentDate("deleted_records.csv")
tableToUpdate = 'records'

calculatedFieldUtil.dayApartUpdate(date, tableToUpdate)
calculatedFieldUtil.hPastPlaceUpdate(date, tableToUpdate)
# must remove invalid r_place now
calculatedFieldUtil.removeInvalidRPlace(del_path, tableToUpdate)
calculatedFieldUtil.rWgtDiffUpdate(date, tableToUpdate)
calculatedFieldUtil.horseEloUpdate(date, tableToUpdate)
calculatedFieldUtil.jockeyEloUpdate(date, tableToUpdate)
calculatedFieldUtil.trainerEloUpdate(date, tableToUpdate)
calculatedFieldUtil.horseWinPercent(date, tableToUpdate)
calculatedFieldUtil.jockeyWinPercent(date, tableToUpdate)
calculatedFieldUtil.trainerWinPercent(date, tableToUpdate)

cur.execute('''select race_id 
            from records order by race_id desc limit 1''')
rows = cur.fetchall()
updated = rows[0][0]
print("All Feature Extracted! Updated Until:", updated)
