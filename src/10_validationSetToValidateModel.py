from src.predicSelectedRaceId import checkPointPath, metaPath
from src.utils import setsForModel, tensorModelUtil, sortResultUtil, predictResultUtil

validationSetTable = 'records_validation_set'

setsForModel.exportValidationSetsWithRPlaceToNewTable(validationSetTable, 'records')
sess, savedGraph = tensorModelUtil.loadSavedModel(metaPath, checkPointPath)
validationSetDf = setsForModel.loadPredictionSetOrderByDraw(validationSetTable)
tensorModelUtil.feedToModelAndPredict(validationSetTable, sess, savedGraph, validationSetDf, checkPointPath)

