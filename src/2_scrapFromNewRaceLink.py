import csv
import datetime
import os
import re

from bs4 import BeautifulSoup
from src.config.webDriver import driver
from src.utils import generatedFileUtil, seleniumUtil

fileDateString = generatedFileUtil.getFileDateString()

dir_path = os.path.dirname(os.path.realpath(__file__))

generatedFilesPath = dir_path + r"\generatedFiles"
outPutPath = dir_path + r"\generatedFiles\2_scrapFromNewRaceLink"

fo = open(generatedFilesPath + rf"\1_generateNewRaceLink\{fileDateString}_newRacelink.txt".replace("\u202a", ""), "r")
# open record.csv with headings
recordsFilePath = outPutPath + generatedFileUtil.generateFileNameWithCurrentDate("records.csv")
open(recordsFilePath, 'w').close()  # clear the previous record data
recordfile = open(recordsFilePath, 'a')
recordWriter = csv.writer(recordfile, lineterminator='\n')

recordWriter.writerow(['race_id', 'r_place', 'r_hr_num', 'horse_id', 'rjockey', 'trainer',
                       'act_wt', 'decalr_wt', 'draw', 'lbw', 'run_pos', 'fin_time', 'win_odds',
                       'h_day_apart', 'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_h_total',
                       'r_j_total', 'r_j_winrate', 'r_t_total', 'r_t_winrate', 'after_h_elo',
                       'before_h_elo', 'after_j_elo', 'before_j_elo', 'after_t_elo', 'before_t_elo',
                       'label'])

# open race.csv with headings
raceFilePath = outPutPath + generatedFileUtil.generateFileNameWithCurrentDate("race.csv")
open(raceFilePath, 'w').close()  # clear the previous race data
racefile = open(raceFilePath.replace("\u202a", ""), 'a')
raceWriter = csv.writer(racefile, lineterminator='\n')
raceWriter.writerow(['race_id', 'r_date', 'r_loc', 'r_num', 'r_class', 'distance',
                     'going', 'course', 'pool'])


def extractno(str):
    number = []
    for word in str:
        if word.isdigit():
            number.append(word)
    number2 = "".join(number)
    return number2


def rmspace(str):
    tmp = []
    for i in range(len(str)):
        try:
            if str[i] == ' ' and str[i + 1] == ' ':
                break
            else:
                tmp.append(str[i])
        except IndexError:
            break

    word = "".join(tmp)
    return word


NO_OF_PAGE_TO_LOAD_CSS_SELECTOR = "body > div > div.f_clear.top_races > table > tbody > tr:nth-child(1) > td:not([class]) img"
LOCATION_AND_DATE_CSS_SELECTOR = "body > div > div.raceMeeting_select > p:nth-child(1) > span.f_fl.f_fs13"
RACE_NUMBER_AND_raceId_CSS_SELECTOR = "body > div > div.race_tab > table > thead > tr > td:nth-child(1)"
CLS_GRP_AND_DISTANCE_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(1)"
# line = fo.readline()
for line in fo:
    # url ="http://racing.hkjc.com/racing/info/meeting/Results/English/Local/20171122/HV"
    # url = "http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171001/ST/7"
    url = line.replace("\n", "")
    tmp = extractno(url)
    tmp2 = datetime.datetime.strptime(tmp, '%Y%m%d').date()
    date = tmp2.strftime('%d/%m/%Y')
    driver.get(url)
    seleniumUtil.checkLoading(driver)

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    # Get number of page to load
    soup2 = soup.select(NO_OF_PAGE_TO_LOAD_CSS_SELECTOR)
    num = len(soup2) - 1

    print("Proceed!")
    print(line)
    print("num = ", num)
    # Generate links to be scrap
    link = []
    s = soup.select(LOCATION_AND_DATE_CSS_SELECTOR)[0].text
    if "Sha Tin" in s:
        location = "ST"
    elif "Happy Valley" in s:
        location = "HV"
    elif "Conghua" in s:
        location = "CH"
    else:
        print("Error in location")
        print(s)
        break
    for i in range(num):
        tmp = url + "/" + location + "/" + str(i + 1)
        link.append(tmp)
    print("link generated")
    print(link)
    # Scrap through url in link
    for curr in range(len(link)):
        driver.get(link[curr])
        seleniumUtil.checkLoading(driver)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        print("Proceed!")
        # check refund
        if "no information" in soup.get_text().lower():
            driver.refresh()
            seleniumUtil.checkLoading(driver)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            print("no information")
        # start scrap
        # Date//Race
        s = soup.select(LOCATION_AND_DATE_CSS_SELECTOR)[0].text
        match = re.search('\d{2}/\d{2}/\d{4}', s)
        date = datetime.datetime.strptime(match.group(), '%d/%m/%Y').date()

        # Race Number//Race
        tmp = soup.select(RACE_NUMBER_AND_raceId_CSS_SELECTOR)[0].text
        i = 0
        for i in range(len(tmp)):
            if tmp[i] == " " and tmp[i + 2] == " ":
                raceNumber = int(tmp[i + 1])
            elif tmp[i] == " " and tmp[i + 3] == " ":
                raceNumber = int(tmp[i + 1]) * 10 + int(tmp[i + 2])

        # raceId//Race + Record
        tmp = list(extractno(str(date)))
        if len(str(raceNumber)) == 1:  # raceNumber 0 - 9
            tmp.append("0" + str(raceNumber))
        elif len(str(raceNumber)) > 1:  # raceNumber > 9
            tmp.append(str(raceNumber))

        raceId = "".join(tmp)
        # r_class//Race
        # Group Three http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171001/ST/7
        # Group Two http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20171022/ST/7
        # Group One http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/20180225/ST/8
        classGroup = soup.select(CLS_GRP_AND_DISTANCE_CSS_SELECTOR)[0].contents[0]
        classGroup = rmspace(classGroup.replace("-", ""))
        # Distance//Race
        tmp = soup.select(CLS_GRP_AND_DISTANCE_CSS_SELECTOR)[0].contents[0]
        dis = re.findall(r"(\d+)M", tmp)[0]
        # Going - Track Condition//Race
        going = soup.select("body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(3)")[0].text
        # Course - Track//Race
        course = soup.select("body > div > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(3)")[
            0].text.replace('"', '')
        # Pool - Prize Pool//Race
        tmp = soup.select("body > div > div.race_tab > table > tbody > tr:nth-child(4) > td:nth-child(1)")[0].text
        listOfMatches = re.findall(r"\d+", tmp)
        pool = "".join(listOfMatches)
        # Number of horses//Record
        table = soup.select("body > div > div.performance > table > tbody > tr")
        numberOfHorse = len(table)
        # Place//Record
        place = []
        for i in range(numberOfHorse):
            place.append(table[i].contents[1].text)
        # Horseid//Record
        horseId = []
        for i in range(numberOfHorse):
            tmp = table[i].contents[5].text
            tmp = re.findall(r"\(([A-Za-z0-9_]+)\)", tmp)[0]
            horseId.append(tmp)
        # Jockey//Record
        jockey = []
        for i in range(numberOfHorse):
            tmp = table[i].contents[7].text
            tmp = re.findall(r"\w+", tmp)
            jockey.append(" ".join(tmp))
        # Trainer//Record
        trainer = []
        for i in range(numberOfHorse):
            tmp = table[i].contents[9].text
            tmp = re.findall(r"\w+", tmp)
            trainer.append(" ".join(tmp))
        # Actual Weight - Carried Weight//Record
        actualWeight = []
        for i in range(numberOfHorse):
            actualWeight.append(table[i].contents[11].text)
        # Declare Weight - Overall Weight//Record
        declareWeight = []
        for i in range(numberOfHorse):
            declareWeight.append(table[i].contents[13].text)
        # Draw//Record
        draw = []
        for i in range(numberOfHorse):
            draw.append(table[i].contents[15].text)
        # LBW - Lengh behind winner//Record
        lbw = []
        for i in range(numberOfHorse):
            lbw.append("abc" + table[i].contents[17].text)
        # Running Position//Record
        runningPosition = []
        for i in range(numberOfHorse):
            tmp = table[i].contents[19].text
            tmp = re.findall(r"\d+", tmp)
            if not tmp:
                runningPosition.append("-")
            else:
                runningPosition.append(" ".join(tmp))
        # Time - Finishing Time//Record
        finishTime = []
        for i in range(numberOfHorse):
            finishTime.append(table[i].contents[21].text)
        # Win Odds - Closing Odds//Record
        winOdds = []
        for i in range(numberOfHorse):
            if len(table[0].contents) < 24:
                winOdds.append("0")
            else:
                winOdds.append(table[i].contents[23].text)
        # output record.csv
        for i in range(numberOfHorse):
            recordWriter.writerow([raceId, place[i], numberOfHorse, horseId[i], jockey[i], trainer[i],
                                   actualWeight[i], declareWeight[i], draw[i], lbw[i], runningPosition[i],
                                   finishTime[i], winOdds[i]])
        # output race.csv
        raceWriter.writerow([raceId, date, location, raceNumber,
                             classGroup, dis, going, course, pool])
recordfile.close()
racefile.close()
fo.close()
driver.quit()
# http://www.hkjc.com/english/racing/results.asp?racedate=27/06/2007
# http://racing.hkjc.com/racing/info/meeting/Results/English/Local/20061223
