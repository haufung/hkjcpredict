import os
import sys
from src import scrapNewHorseData
from src.config.horseRaceDataDbConnection import connection
from src.utils import generatedFileUtil

DATE_STRING = generatedFileUtil.getFileDateString()
root_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(root_path)

inputPath = root_path + r"\generatedFiles\2_scrapFromNewRaceLink"
scrapNewHorseDataPath = root_path + r"\generatedFiles\scrapNewHorseData"
local_save_path = scrapNewHorseDataPath + r"\localSave"


print("Opened database successfully")
cur = connection.cursor()

cur.execute(r'''begin;''')

# import records.csv
cur.execute(rf'''COPY tmp_records(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,
run_pos,fin_time,win_odds) 
FROM '{inputPath}\{DATE_STRING}_records.csv' DELIMITER ',' CSV HEADER;''')

print("imported records.csv to tmp_record")

cur.execute(r'''select distinct horse_id from tmp_records 
  where (horse_id) not in
   ( select horse_id from horse )''')

rows = cur.fetchall()
cur.execute(r'''rollback;''')
nw_horse = []
for item in rows:
    nw_horse.append(item[0])

if not nw_horse:
    print("No new horse found")
else:
    print("New horse found:")
    for horse in nw_horse:
        print(horse)
    print("Scraping new horse data...")
    scrapNewHorseData.gen_link(nw_horse, scrapNewHorseDataPath, local_save_path)
    scrapNewHorseData.scrap_horse(scrapNewHorseDataPath, local_save_path)
    # import horse.csv
    cur.execute(rf'''COPY horse(horse_id,name,origin,age,colour,sex,import_type,sire,dam,dam_sire)
    FROM '{scrapNewHorseDataPath}\{DATE_STRING}_horse.csv' DELIMITER ',' CSV HEADER;''' % scrapNewHorseDataPath)
    print("imported horse.csv")

# import race.csv
cur.execute(rf'''COPY race(race_id,r_date,r_loc,r_num,r_class,distance,going,course,pool)
FROM '{inputPath}\{DATE_STRING}_race.csv' DELIMITER ',' CSV HEADER;''')

print("imported race.csv")

# import records.csv
cur.execute(rf'''COPY records(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,run_pos,
fin_time,win_odds) 
FROM '{inputPath}\{DATE_STRING}_records.csv' DELIMITER ',' CSV HEADER;''')

print("imported records.csv")
