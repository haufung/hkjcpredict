import os
import sys
from src.utils import sqlQueryUtil, nullDataUtil

root_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(root_path)

sql_path = root_path + r"\config\nullDataUpdate"
recordsTable = 'records'
nullDataUtil.hDayApartUpdate(recordsTable)
nullDataUtil.rWgtDiffUpdate(recordsTable)
nullDataUtil.hPastPlaceUpdate(recordsTable)

sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\horse_age.txt")
sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\records_label.txt")

print("Null data handled! ")
