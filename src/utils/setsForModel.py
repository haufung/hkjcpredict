
from src.config.horseRaceDataDbConnection import connection
import pandas as pd

cur = connection.cursor()


def outPutValidationSet(recordsTable, outputPath):
    cur.execute(rf'''Copy (select {recordsTable}.race_id,r_loc, distance, going, course,act_wt, decalr_wt, draw, age, 
h_day_apart, h_past_place, r_wgt_diff, r_h_winrate, r_j_winrate, r_t_winrate, before_h_elo, before_j_elo, 
before_t_elo, label from 
{recordsTable},race,horse where race.race_id = {recordsTable}.race_id and horse.horse_id = {recordsTable}.horse_id and r_date > (select 
r_date from race order by r_date desc limit 1) - interval '1 year' order by {recordsTable}.race_id::integer desc, draw::integer asc) To '{outputPath}' 
With CSV DELIMITER ',' HEADER;''')

def outPutTestingSet(recordsTable, outputPath):
    cur.execute(rf'''Copy (select {recordsTable}.race_id,r_loc, distance, going, course,act_wt, decalr_wt, draw, age, 
h_day_apart, h_past_place, r_wgt_diff, r_h_winrate, r_j_winrate, r_t_winrate, before_h_elo, before_j_elo, 
before_t_elo, label 
from {recordsTable},race,horse 
where race.race_id = {recordsTable}.race_id 
and horse.horse_id = {recordsTable}.horse_id 
and r_date > (select r_date from race order by r_date desc limit 1) - interval '2 year' 
and r_date < (select r_date from race order by r_date desc limit 1) - interval '1 year' 
order by {recordsTable}.race_id::integer desc, draw::integer asc) To '{outputPath}' 
With CSV DELIMITER ',' HEADER;''')

def outPutTrainingSet(recordsTable, outputPath):
    cur.execute(rf'''Copy (select {recordsTable}.race_id,r_loc, distance, going, course,act_wt, decalr_wt, draw, age, 
h_day_apart, h_past_place, r_wgt_diff, r_h_winrate, r_j_winrate, r_t_winrate, before_h_elo, before_j_elo, 
before_t_elo, label 
from {recordsTable},race,horse 
where race.race_id = {recordsTable}.race_id 
and horse.horse_id = {recordsTable}.horse_id 
and r_date < (select r_date from race order by r_date desc limit 1) - interval '1 year' 
order by {recordsTable}.race_id::integer desc, draw::integer asc) To '{outputPath}' 
With CSV DELIMITER ',' HEADER;''')

def loadPredictionSetOrderByDraw(recordsTable):
    return pd.read_sql(rf'''select {recordsTable}.race_id, horse.horse_id, r_loc, distance, going, course,act_wt::integer, decalr_wt::integer, draw, age, 
h_day_apart::integer, h_past_place, r_wgt_diff::integer, r_h_winrate::float, r_j_winrate::float, r_t_winrate::float, before_h_elo::float, before_j_elo::float, 
before_t_elo::float 
from {recordsTable},race,horse 
where race.race_id = {recordsTable}.race_id 
and horse.horse_id = {recordsTable}.horse_id
order by {recordsTable}.race_id::integer asc, draw::integer asc''', connection)

def exportValidationSetsWithRPlaceToNewTable(tableName, recordsTable):
    cur.execute(rf'''create table IF NOT EXISTS {tableName} as select * from {recordsTable}, 
(select distinct race_id as race_race_id from race where r_date > (select r_date from race order by r_date desc limit 1) - interval '1 year') as alias 
where alias.race_race_id = {recordsTable}.race_id order by {recordsTable}.race_id::integer desc, draw::integer asc;''')
    connection.commit()