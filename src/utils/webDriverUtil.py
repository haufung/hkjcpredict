import os

from selenium import webdriver

def webDriverSetup():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    options = webdriver.ChromeOptions()
    options.binary_location = "C:\Program Files\Google\Chrome\Application\chrome.exe"
    options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
    options.add_argument('--disable-gpu')
    chrome_driver_path = dir_path + r'\chromedriver.exe'
    return options, chrome_driver_path
