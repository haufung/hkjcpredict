import pandas as pd
import tensorflow as tf

from src.utils import picklistValueUtil


def predict_sam(sample, i):
    with open(sample.replace('\u202a', ""), 'r') as csvfile:
        df = pd.read_csv(csvfile, nrows=20)
        this_dict = df.loc[[i]].to_dict('list')
        this_dict['distance'][0] = str(this_dict['distance'][0])
        this_dict['age'][0] = str(this_dict['age'][0])
        this_dict['draw'][0] = str(this_dict['draw'][0])
        this_dict['h_past_place'][0] = str(this_dict['h_past_place'][0])
        del this_dict['label']

    return this_dict


def get_feature_column(cpPath):
    raceRLocPicklist = picklistValueUtil.raceRLocPicklist()
    raceDistancePicklist = picklistValueUtil.raceDistancePicklist()
    raceGoingPicklist = picklistValueUtil.raceGoingPicklist()
    raceCoursePicklist = picklistValueUtil.raceCoursePicklist()
    recordsDrawPicklist = picklistValueUtil.recordsDrawPicklist()
    horseAgePicklist = picklistValueUtil.horseAgePicklist()
    recordsHPastPlacePicklist = picklistValueUtil.recordsHPastPlacePicklist()
    r_loc = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'r_loc', raceRLocPicklist))
    distance = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'distance', raceDistancePicklist))
    going = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'going', raceGoingPicklist))
    course = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'course', raceCoursePicklist))
    act_wt = tf.feature_column.numeric_column(
        'act_wt')
    decalr_wt = tf.feature_column.numeric_column(
        'decalr_wt')
    draw = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'draw', recordsDrawPicklist))
    age = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'age', horseAgePicklist))
    h_day_apart = tf.feature_column.numeric_column(
        'h_day_apart')
    h_past_place = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list(
        'h_past_place', recordsHPastPlacePicklist))
    r_wgt_diff = tf.feature_column.numeric_column(
        'r_wgt_diff')
    r_h_winrate = tf.feature_column.numeric_column(
        'r_h_winrate')
    r_j_winrate = tf.feature_column.numeric_column(
        'r_j_winrate')
    r_t_winrate = tf.feature_column.numeric_column(
        'r_t_winrate')
    before_h_elo = tf.feature_column.numeric_column(
        'before_h_elo')
    before_j_elo = tf.feature_column.numeric_column(
        'before_j_elo')
    before_t_elo = tf.feature_column.numeric_column(
        'before_t_elo')
    r_loc_x_draw_x_course = tf.feature_column.crossed_column(
        [tf.feature_column.categorical_column_with_vocabulary_list(
            'r_loc', raceRLocPicklist)
            , tf.feature_column.categorical_column_with_vocabulary_list(
            'draw', recordsDrawPicklist),
            tf.feature_column.categorical_column_with_vocabulary_list(
                'course', raceCoursePicklist)], 50000)
    if cpPath is not None:
        r_loc_x_draw_x_course_embedded = tf.feature_column.embedding_column(r_loc_x_draw_x_course, 50, ckpt_to_load_from=cpPath, tensor_name_in_ckpt=r"input_layer_1/course_X_draw_X_r_loc_embedding/embedding_weights")
    else:
        r_loc_x_draw_x_course_embedded = tf.feature_column.embedding_column(r_loc_x_draw_x_course, 50)
    my_feature_column = [r_loc, distance, going, course, act_wt, decalr_wt
        , draw, age, h_day_apart, h_past_place, r_wgt_diff, r_h_winrate
        , r_j_winrate, r_t_winrate, before_h_elo, before_j_elo
        , before_t_elo, r_loc_x_draw_x_course_embedded]

    return my_feature_column
