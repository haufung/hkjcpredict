import datetime

DATE_FORMAT = '%Y%m%d'


def generateFileNameWithCurrentDate(fileNameString):
    x = datetime.datetime.today()
    dateString = x.strftime(DATE_FORMAT)
    return f'\{dateString}_{fileNameString}'


def getFileDateString():
    x = datetime.datetime.today()
    dateString = x.strftime(DATE_FORMAT)
    return dateString
