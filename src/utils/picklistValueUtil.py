from src.config.horseRaceDataDbConnection import connection

cur = connection.cursor()

DB_TO_STORE_PICKLIST = 'config.'


def horseAgePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}horse_age_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceCoursePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_course_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceDistancePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_distance_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceGoingPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_going_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceRLocPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_r_loc_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def recordsDrawPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}records_draw_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def recordsHPastPlacePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}records_h_past_place_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist
