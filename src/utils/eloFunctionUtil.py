from src.config.horseRaceDataDbConnection import connection
cur = connection.cursor()

def declareHorseScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION IF EXISTS sf(text,text);
    CREATE OR REPLACE FUNCTION sf(hor_id text, rid text)
    RETURNS text AS 
    $$
    declare  sf double precision;
        nw_hr_num integer := (select coalesce((select count(race_id)/2  from {recordsTable} where r_place like '% DH' and race_id = rid group by race_id), 0))::integer;
        n integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id) - nw_hr_num;
    BEGIN

     select case when r_place like '% DH'
     then (n - (select coalesce(substring(r_place from '(..) DH'), substring(r_place from '(.) DH')))::float+0.5)/((n*(n-1))/2)
     else  (n - r_place::float )/((n*(n-1))/2)
     end as sf from {recordsTable} where race_id = rid and horse_id = hor_id
       into sf;
       RETURN sf;

    END;
    $$ 
    LANGUAGE plpgsql;''')

def declareHorseEstimateScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION IF EXISTS esf(text,text,text);
    CREATE OR REPLACE FUNCTION esf(hor_id text, rid text , this_before_elo text)
    RETURNS text AS $$
    declare
        esf float;
        r_hr_num integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id);
    BEGIN

    select alias.nom/((r_hr_num::float*(r_hr_num::float - 1))/2) as esf from (
    select sum(1/(1+ "power10"(before_h_elo::float,this_before_elo::float)))::float as nom from {recordsTable} where 
    race_id = rid and
    horse_id != hor_id 
    group by race_id) as alias

       into esf;
       RETURN esf;
       exception when numeric_value_out_of_range then 
            raise exception 'Value out of range for hor_id = % rid= % this_before_elo= % r_hr_num= %  esf= %', hor_id, rid, this_before_elo, r_hr_num, esf;
    END;
    $$ LANGUAGE plpgsql;''')

def declareHorseEloFunction(recordsTable):
    cur.execute(rf'''CREATE OR REPLACE FUNCTION compute_elo_k(nk integer, rid text)
        RETURNS void AS $$

        BEGIN 
        update {recordsTable}  
        set before_h_elo = alias.past_elo
        from (select "past_elo"(horse_id, race_id),race_id,horse_id from {recordsTable} where race_id = rid) as alias
        where alias.race_id = {recordsTable}.race_id and alias.horse_id = {recordsTable}.horse_id;

        update {recordsTable} 
        set after_h_elo = alias3.before_h_elo::float + nk*(alias2.sf::float - alias1.esf::float)
        from
        (select "esf"(horse_id, race_id, before_h_elo),horse_id from {recordsTable} where  race_id = rid) as alias1,
        (select "sf"(horse_id, race_id),horse_id from {recordsTable} where race_id = rid) as alias2,
        (select before_h_elo,horse_id from {recordsTable} where race_id = rid) as alias3
        where {recordsTable}.horse_id = alias1.horse_id 
        and {recordsTable}.horse_id = alias2.horse_id 
        and {recordsTable}.horse_id = alias3.horse_id
        and {recordsTable}.race_id = rid;

        END;
        $$ LANGUAGE plpgsql;''')

def declareJockeyScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION sf(text,text);
    CREATE OR REPLACE FUNCTION sf(jockey text, rid text)
    RETURNS text AS $$
    declare
        sf double precision;
        nw_hr_num integer := (select coalesce((select count(race_id)/2  from {recordsTable} where r_place like '% DH' and race_id = rid group by race_id), 0))::integer;
        n integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id) - nw_hr_num;
    BEGIN
    
    
     select case when r_place like '% DH'
     then (n - (select coalesce(substring(r_place from '(..) DH'), substring(r_place from '(.) DH')))::float+0.5)/((n*(n-1))/2)
     else  (n - r_place::float )/((n*(n-1))/2)
     end as sf from {recordsTable} where race_id = rid and rjockey = jockey
       into sf;
       RETURN sf;
    
    END;
    $$ LANGUAGE plpgsql;''')

def declareJockeyEstimateScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION esf(text,text,text);
    CREATE OR REPLACE FUNCTION esf(jockey text, rid text , this_before_elo text)
    RETURNS text AS $$
    declare
        esf float;
        r_hr_num integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id);
    BEGIN
    
    select alias.nom/((r_hr_num::float*(r_hr_num::float - 1))/2) as esf from (
    select sum(1/(1+ "power10"(before_j_elo::float,this_before_elo::float)))::float as nom from {recordsTable} where 
    race_id = rid and
    rjockey != jockey
    group by race_id) as alias
    
       into esf;
       RETURN esf;
       exception when numeric_value_out_of_range then 
            raise exception 'Value out of range for rjockey = % rid= % this_before_elo= % r_hr_num= %  esf= %', rjockey, rid, this_before_elo, r_hr_num, esf;
    END;
    $$ LANGUAGE plpgsql;''')

def declareJockeyEloFunction(recordsTable):
    cur.execute(rf'''CREATE OR REPLACE FUNCTION compute_elo_k(nk integer, rid text)
    RETURNS void AS $$
    
    BEGIN 
    update {recordsTable}  
    set before_j_elo = alias.past_elo
    from (select "past_elo"(rjockey, race_id),race_id,rjockey from {recordsTable} where race_id = rid) as alias
    where alias.race_id = {recordsTable}.race_id and alias.rjockey = {recordsTable}.rjockey;
    
    
    update {recordsTable} 
    set after_j_elo = alias3.before_j_elo::float + nk*(alias2.sf::float - alias1.esf::float)
    from
    (select "esf"(rjockey, race_id, before_j_elo) as esf,rjockey from {recordsTable} where  race_id = rid) as alias1,
    (select "sf"(rjockey, race_id) as sf,rjockey from {recordsTable} where  race_id = rid) as alias2,
    (select before_j_elo,rjockey from {recordsTable} where race_id = rid) as alias3
    where {recordsTable}.rjockey = alias1.rjockey
    and {recordsTable}.rjockey = alias2.rjockey
    and {recordsTable}.rjockey = alias3.rjockey
    and {recordsTable}.race_id = rid;
    
    END;
    $$ LANGUAGE plpgsql;''')

def declareTrainerScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION t_sf(text,text);
    CREATE OR REPLACE FUNCTION t_sf(rtrainer text, rid text)
    RETURNS text AS $$
    declare
        sf double precision;
        nw_hr_num integer := (select coalesce((select count(race_id)/2  from {recordsTable} where r_place like '% DH' and race_id = rid group by race_id), 0))::integer;
        n integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id) - nw_hr_num;
    BEGIN
    
    
     select case when r_place like '% DH'
     then (n - (select coalesce(substring(r_place from '(..) DH'), substring(r_place from '(.) DH')))::float+0.5)/((n*(n-1))/2)
     else  (n - r_place::float )/((n*(n-1))/2)
     end as sf from {recordsTable} where race_id = rid and trainer = rtrainer
       into sf;
       RETURN sf;
    
    END;
    $$ LANGUAGE plpgsql;''')

def declareTrainerEstimateScoreFunction(recordsTable):
    cur.execute(rf'''DROP FUNCTION t_esf(text,text,text);
    CREATE OR REPLACE FUNCTION t_esf(rtrainer text, rid text , this_before_elo text)
    RETURNS text AS $$
    declare
        esf float;
        r_hr_num integer := (select count(race_id)::integer from {recordsTable} where race_id = rid group by race_id);
    BEGIN
    
    select alias.nom/((r_hr_num::float*(r_hr_num::float - 1))/2) as esf from (
    select sum(1/(1+ "power10"(before_t_elo::float,this_before_elo::float)))::float as nom from {recordsTable} where 
    race_id = rid and
    trainer != rtrainer
    group by race_id) as alias
    
       into esf;
       RETURN esf;
       exception when numeric_value_out_of_range then 
            raise exception 'Value out of range for trainer = % rid= % this_before_elo= % r_hr_num= %  ', trainer, rid, this_before_elo, r_hr_num;
    END;
    $$ LANGUAGE plpgsql;''')

def declareTrainerEloFunction(recordsTable):
    cur.execute(rf'''CREATE OR REPLACE FUNCTION compute_elo_k(nk integer ,rid text)
    RETURNS void AS $$
    
    BEGIN 
    update {recordsTable}  
    set before_t_elo = alias.t_past_elo
    from (select "t_past_elo"(trainer, race_id),race_id,trainer from {recordsTable} where race_id = rid) as alias
    where alias.race_id = {recordsTable}.race_id and alias.trainer = {recordsTable}.trainer;
    
    update {recordsTable} 
    set after_t_elo = alias3.before_t_elo::float + nk*(alias2.t_sf::float - alias1.t_esf::float)
    from
    (select "t_esf"(trainer, race_id, before_t_elo),trainer from {recordsTable} where  race_id = rid) as alias1,
    (select "t_sf"(trainer, race_id),trainer from {recordsTable} where  race_id = rid) as alias2,
    (select before_t_elo,trainer from {recordsTable} where race_id = rid) as alias3
    where {recordsTable}.trainer = alias1.trainer
    and {recordsTable}.trainer = alias2.trainer
    and {recordsTable}.trainer = alias3.trainer
    and {recordsTable}.race_id = rid;
    
    END;
    $$ LANGUAGE plpgsql;''')