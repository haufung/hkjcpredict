import pandas as pd
import tensorflow as tf
import numpy as np
from src.utils import generatedFileUtil
import random
import copy

TRAIN_COLUMN_NAMES = ['race_id', 'r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age', 'h_day_apart',
                      'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate', 'before_h_elo',
                      'before_j_elo', 'before_t_elo', 'label']
TEST_COLUMN_NAMES = ['race_id', 'r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age', 'h_day_apart',
                     'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate', 'before_h_elo',
                     'before_j_elo', 'before_t_elo', 'label']


def label_to_class(series):
    series.loc[-1] = np.int64(1)
    series.loc[-2] = np.int64(0)
    series = pd.get_dummies(series)
    series = series.drop(series.index[[-1, -2]])

    return series


def shuffleByRaceId(df):
    groups = [df for _, df in df.groupby('race_id')]
    random.shuffle(groups)
    dfShuffledByRaceId = pd.concat(groups).reset_index(drop=True)
    return dfShuffledByRaceId


def loadFromCsv(path, columnNames):
    dataDf = pd.read_csv(path, names=columnNames, header=0,
                       converters={'race_id': str,
                                   'h_past_place': str,
                                   'distance': str,
                                   'age': str,
                                   'draw': str})
    return dataDf

def createBatchForInputLayer(data, featureColumn):
    input_this_x = tf.feature_column.input_layer(dict(data), featureColumn)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        this_x_to_feed = sess.run(input_this_x)
    return this_x_to_feed

def generateKeyValueBatch(data, featureColumn):
    _ = data.pop('race_id')
    this_x, this_y = data, data.pop('label')
    this_y = label_to_class(this_y).values
    this_x_to_feed = createBatchForInputLayer(this_x, featureColumn)
    return this_x_to_feed, this_y


def load_data(path, my_feature_column):
    train_path = path + generatedFileUtil.generateFileNameWithCurrentDate("trainingSet.csv")
    test_path = path + generatedFileUtil.generateFileNameWithCurrentDate("testingSet.csv")
    val_test_path = path + generatedFileUtil.generateFileNameWithCurrentDate("validationSet.csv")

    train = loadFromCsv(train_path, TRAIN_COLUMN_NAMES)
    # Shuffle all data and reset the row number
    trainGroupByRaceId = shuffleByRaceId(train)
    batchSizeMapping = train.groupby("race_id").size()
    anotherTrainGroupByRaceId = copy.deepcopy(trainGroupByRaceId)
    trainGroupByRaceId = trainGroupByRaceId.values.tolist()
    train_x, train_y = generateKeyValueBatch(anotherTrainGroupByRaceId, my_feature_column)

    test = loadFromCsv(test_path, TEST_COLUMN_NAMES)
    test_batch = test.groupby("race_id", sort=False).size()
    test_x, test_y = generateKeyValueBatch(test, my_feature_column)

    val_test = loadFromCsv(val_test_path, TEST_COLUMN_NAMES)
    val_test_batch = val_test.groupby("race_id", sort=False).size()
    val_test_x, val_test_y = generateKeyValueBatch(val_test, my_feature_column)

    return (train_x, train_y, batchSizeMapping, trainGroupByRaceId), (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch)