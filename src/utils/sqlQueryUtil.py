from src.config.horseRaceDataDbConnection import connection

cur = connection.cursor()


def executeSqlQueryFromTxt(path):
    fo = open(path, 'r')
    sqlFile = fo.read()
    fo.close()
    ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
    cur.execute(ln_sql)
    connection.commit()

