
from src.config.horseRaceDataDbConnection import connection
cur = connection.cursor()

def hDayApartUpdate(recordsTable):
    cur.execute(rf'''update {recordsTable} set h_day_apart_is_null = true where h_day_apart is null;
    update {recordsTable} set h_day_apart = '126' where h_day_apart is null;''')
    print("null h_day_apart update completed!")
    connection.commit()

def hPastPlaceUpdate(recordsTable):
    cur.execute(rf'''UPDATE {recordsTable} set h_past_place_is_null = true where h_past_place is null;
    UPDATE {recordsTable} set h_past_place = 'NULL' where h_past_place_is_null = true;''')
    print("null h_past_place update completed!")
    connection.commit()

def rWgtDiffUpdate(recordsTable):
    cur.execute(rf'''update {recordsTable} set r_wgt_diff_is_null = true where r_wgt_diff is null;
    update {recordsTable} set r_wgt_diff = '32' where r_wgt_diff is null;''')
    print("null r_wgt_diff update completed!")
    connection.commit()