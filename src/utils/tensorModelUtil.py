import numpy as np
import tensorflow as tf

from src.utils import tensorflowDataPreProcess, \
    tensorflowFeatureColumn, predictResultUtil, sortResultUtil


def loadSavedModel(meta, cpPath):
    session = tf.Session()
    saver = tf.train.import_meta_graph(meta)
    saver.restore(session, cpPath)
    graphWithVariable = tf.get_default_graph()
    return session, graphWithVariable



def feedToModelAndPredict(tableName, session, graph, predicSetDf, checkPointPath):

    x = graph.get_tensor_by_name("x:0")
    pred = graph.get_tensor_by_name("pred1:0")
    batchSizeForEachRaceId = predicSetDf.groupby("race_id", sort=False).size()
    raceIdList, horseIdList = predicSetDf.pop('race_id'), predicSetDf.pop('horse_id')
    featureColumn = tensorflowFeatureColumn.get_feature_column(checkPointPath)
    predicSetToFeedAllBatches = tensorflowDataPreProcess.createBatchForInputLayer(predicSetDf, featureColumn)

    i = 0
    for thisBatch in batchSizeForEachRaceId:
        resultList = []
        start = i
        end = i + thisBatch
        thisBatchToFeed = np.array(predicSetToFeedAllBatches[start:end])
        thisBatchResult = []
        try:
            thisBatchResult = session.run(pred, feed_dict={x: thisBatchToFeed})
        except Exception as e:
            print(e)
        for j in range(len(thisBatchResult)):
            resultList.append([raceIdList[i+j], horseIdList[i+j], j + 1, thisBatchResult[j][1]])
            if tableName is not None:
                predictResultUtil.updateConfidenceInDB(tableName, raceIdList[i+j], horseIdList[i+j], thisBatchResult[j][1])
        i += thisBatch

        sortedResult = sorted(resultList, key=sortResultUtil.takeConfidence, reverse=True)
        for k in range(len(sortedResult)):
            print(
                f"raceId: {sortedResult[k][0]} | horseId: {sortedResult[k][1]} | draw: {sortedResult[k][2]} | confidence: {sortedResult[k][3]} | predicted place: {k + 1}")
            if tableName is not None:
                predictResultUtil.updatePredictedPlaceInDB(tableName, sortedResult[k][0], sortedResult[k][1], k + 1)
