from src.config.horseRaceDataDbConnection import connection

cur = connection.cursor()


def updateConfidenceInDB(tableName, raceId, horseId, confidence):
    cur.execute(rf'''ALTER TABLE {tableName}
    ADD COLUMN IF NOT EXISTS confidence double precision;''')
    cur.execute(
        rf'''update {tableName} set confidence = {confidence} where race_id = '{raceId}' and horse_id = '{horseId}' ''')
    connection.commit()


def updatePredictedPlaceInDB(tableName, raceId, horseId, predictedPlace):
    cur.execute(rf'''ALTER TABLE {tableName}
    ADD COLUMN IF NOT EXISTS predicted_place integer;''')
    cur.execute(
        rf'''update {tableName} set predicted_place = {predictedPlace} where race_id = '{raceId}' and horse_id = '{horseId}' ''')
    connection.commit()
