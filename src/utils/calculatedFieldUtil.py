import os

from src.config.horseRaceDataDbConnection import connection

from src.utils import sqlQueryUtil, eloFunctionUtil

cur = connection.cursor()

root_path = os.path.dirname(os.path.realpath(__file__))
sql_path = root_path + r"\..\config\calculatedFieldSql"


def dayApartUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\day_apart_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set h_day_apart = alias.day_apart
    from (select  "day_apart"({recordsTable}.horse_id, {recordsTable}.race_id) as day_apart, horse_id, r_date, race.race_id from race, {recordsTable} 
    where race.race_id = {recordsTable}.race_id and {recordsTable}.race_id::integer > {date}) as alias
    where {recordsTable}.race_id = alias.race_id and {recordsTable}.horse_id = alias.horse_id;''')
    print("day_apart update completed!")
    connection.commit()

def hPastPlaceUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\past_place_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set h_past_place = alias.past_place
    from (select  "past_place"({recordsTable}.horse_id, race.race_id, race.course) as past_place, {recordsTable}.race_id, {recordsTable}.horse_id, race.course from race, {recordsTable} 
    where race.race_id = {recordsTable}.race_id and {recordsTable}.race_id::integer > {date}) as alias , race
    where {recordsTable}.race_id = alias.race_id and {recordsTable}.horse_id = alias.horse_id and race.course = alias.course ;''')
    print("past_place update completed!")
    connection.commit()

def rWgtDiffUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\wgt_diff_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set r_wgt_diff  = alias.wgt_diff
    from (select  "r_wgt_diff"({recordsTable}.horse_id, race.race_id, {recordsTable}.decalr_wt) as wgt_diff, {recordsTable}.race_id, {recordsTable}.horse_id, {recordsTable}.decalr_wt from race, {recordsTable} 
    where race.race_id = {recordsTable}.race_id and {recordsTable}.race_id::integer > {date}) as alias , race
    where {recordsTable}.race_id = alias.race_id and {recordsTable}.horse_id = alias.horse_id and {recordsTable}.decalr_wt = alias.decalr_wt;''')
    print("wgt_diff update completed!")
    connection.commit()


def horseEloUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\horse_elo_func.txt")
    eloFunctionUtil.declareHorseScoreFunction(recordsTable)
    eloFunctionUtil.declareHorseEstimateScoreFunction(recordsTable)
    eloFunctionUtil.declareHorseEloFunction(recordsTable)
    cur.execute(rf'''select "compute_elo_k"(2250,alias.race_id)
         from (select distinct race_id from {recordsTable} 
         where race_id::integer > {date} order by race_id) as alias;''')
    print("horse_elo update completed!")
    connection.commit()

def jockeyEloUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\jockey_elo_func.txt")
    eloFunctionUtil.declareJockeyScoreFunction(recordsTable)
    eloFunctionUtil.declareJockeyEstimateScoreFunction(recordsTable)
    eloFunctionUtil.declareJockeyEloFunction(recordsTable)
    cur.execute(rf'''select "compute_elo_k"(50,alias.race_id) 
        from (select distinct race_id from {recordsTable} 
        where race_id::integer > {date} order by race_id) as alias;''')
    print("jockey_elo update completed!")
    connection.commit()

def trainerEloUpdate(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\trainer_elo_func.txt")
    eloFunctionUtil.declareTrainerScoreFunction(recordsTable)
    eloFunctionUtil.declareTrainerEstimateScoreFunction(recordsTable)
    eloFunctionUtil.declareTrainerEloFunction(recordsTable)
    cur.execute(rf'''select "compute_elo_k"(1000,alias.race_id) 
        from (select distinct race_id from {recordsTable} 
        where race_id::integer > {date} order by race_id) as alias;''')
    print("trainer_elo update completed!")
    connection.commit()

def horseWinPercent(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\win_percent_horse_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set r_h_winrate  = alias.r_h_winrate
    from (select  "r_h_winrate"({recordsTable}.horse_id, {recordsTable}.race_id) as r_h_winrate, race_id, horse_id from {recordsTable} 
    where {recordsTable}.race_id::integer > {date}) as alias
    where {recordsTable}.horse_id = alias.horse_id 
    and {recordsTable}.race_id = alias.race_id;''')
    print("win_percent_horse update completed!")
    connection.commit()

def jockeyWinPercent(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\win_percent_jockey_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set r_j_winrate  = alias.r_j_winrate
    from (select  "r_j_winrate"({recordsTable}.rjockey, {recordsTable}.race_id) as r_j_winrate, race_id, rjockey from {recordsTable} 
    where {recordsTable}.race_id::integer > {date}) as alias 
    where {recordsTable}.rjockey = alias.rjockey 
    and {recordsTable}.race_id = alias.race_id;''')
    print("win_percent_jockey update completed!")
    connection.commit()

def trainerWinPercent(date, recordsTable):
    sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\win_percent_trainer_func.txt")
    cur.execute(rf'''update {recordsTable} 
    set r_t_winrate  = alias.r_t_winrate
    from (select  "r_t_winrate"({recordsTable}.trainer, {recordsTable}.race_id) as r_t_winrate, race_id, trainer from {recordsTable} 
    where {recordsTable}.race_id::integer > {date}) as alias 
    where {recordsTable}.trainer = alias.trainer
    and {recordsTable}.race_id = alias.race_id;''')
    print("win_percent_trainer update completed!")
    connection.commit()

def removeInvalidRPlace(savePath, recordsTable):
    if savePath is not None:
        cur.execute(rf'''Copy (select * from {recordsTable} where
        r_place = 'WV'
        or r_place = 'WV-A'
        or r_place ='WX'
        or r_place ='PU'
        or r_place ='UR'
        or r_place ='WX-A'
        or r_place ='FE'
        or r_place ='DNF'
        or r_place ='TNP'
        or r_place ='DISQ'
        or r_place ='WXNR'
        or r_place is null) to '{savePath}' with csv''')
        print("Saved all useless records")

    cur.execute(rf'''delete FROM {recordsTable} where
    r_place = 'WV'
    or r_place = 'WV-A'
    or r_place ='WX'
    or r_place ='PU'
    or r_place ='UR'
    or r_place ='WX-A'
    or r_place ='FE'
    or r_place ='DNF'
    or r_place ='TNP'
    or r_place ='DISQ'
    or r_place ='WXNR'
    or r_place is null;''')
    print("Deleted useless records")
    connection.commit()