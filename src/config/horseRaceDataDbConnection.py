import os
import psycopg2
root_path = os.path.dirname(os.path.realpath(__file__))

with open(root_path + '/credentials') as f:
    credentials = [x.strip().split(':', 5) for x in f]

for database, username, password, host, port, cloud_backup_path in credentials:
    connection = psycopg2.connect(database=database, user=username, password=password, host=host, port=port)