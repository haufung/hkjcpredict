/*#############10 to power of function(avoid float over/underflow)################*/
CREATE OR REPLACE FUNCTION power10(no1 float, no2 float)
RETURNS float AS $$
declare
    result1 float;
BEGIN

select case 
when (no1 - no2)/400 < -309 then 0
when (no1 - no2)/400 > 308 then power(10,308)
else power(10,(no1 - no2)/400) end as result1

into result1;
   RETURN result1;
END;
$$ LANGUAGE plpgsql;

/*############Return 1500 if not find past elo############*/
DROP FUNCTION t_past_elo(text,text);
CREATE OR REPLACE FUNCTION t_past_elo(rtrainer text, rid text)
RETURNS text AS $$
declare
	past_elo text;
BEGIN

SELECT
coalesce(
(SELECT 
after_t_elo::float from records where trainer = rtrainer
 and race_id::integer < rid::integer
 group by race_id,after_t_elo
 order by race_id::integer desc limit 1),1500)
   into past_elo;
   RETURN past_elo;
END;
$$ LANGUAGE plpgsql;
