import csv
import re
import time

from bs4 import BeautifulSoup
from selenium import webdriver
from src.utils import generatedFileUtil

horse_id_and_name_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > span"
origin_and_age_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3)"
colour_and_sex_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3)"
import_type_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3)"
sire_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(5) > td:nth-child(3) > a"
another_sire_selector = 'body > div > div > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(3) > td:nth-child(3)'
dam_name_selector = 'body > div > div > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(4) > td:nth-child(1)'
dam_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(6) > td:nth-child(3)"
another_dam_selector = 'body > div > div > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(4) > td:nth-child(3)'
dam_sire_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(7) > td:nth-child(3)"
another_dam_sire_selector = 'body > div > div > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr:nth-child(5) > td:nth-child(3)'
trackwork_records_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td.table_eng_text > ul > li:nth-child(4) > a"
veterinary_records_selector = "body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td.table_eng_text > ul > li:nth-child(5) > a"


def gen_link(the_list, path, local_save_path):
    browser = webdriver.Chrome()
    search_horse_url = "http://racing.hkjc.com/racing/information/english/Horse/SelectHorse.aspx"
    link = []
    for item in the_list:
        browser.get(search_horse_url)
        checkLoading(browser)
        form = browser.find_element_by_css_selector('table.bigborder input[name="BrandNumber"]')
        form.send_keys(item)
        button = browser.find_element_by_css_selector('input#submit1.table_eng_text')
        button.click()
        link.append(browser.current_url)
    filePath = path + generatedFileUtil.generateFileNameWithCurrentDate("horselink.txt")
    localSaveFilePath = local_save_path + generatedFileUtil.generateFileNameWithCurrentDate("horselink.txt")
    fo = open(filePath, 'w')
    if local_save_path is not None:
        lfo = open(localSaveFilePath, 'a')
    for item in link:
        fo.write(item + "\n")
        if local_save_path is not None:
            lfo.write(item + "\n")
    fo.close()
    if local_save_path is not None:
        lfo.close()
    browser.close()
    return


def extractword(string):
    tmp = []
    this = [":", "\r", "\n", "\t"]
    for i in range(len(string)):
        try:
            if string[i] == "/":
                break
            elif string[i] in this:
                continue
            elif string[i] == " " and string[i - 1] == " ":
                continue
            elif string[i] == " " and (string[i + 1] == " " or string[i + 1] in this):
                continue
            else:
                tmp.append(string[i])
        except IndexError:
            tmp.append(string[i])
            break
    word = "".join(tmp)
    return word


# within /abc/
def extractword2(string):
    tmp = []
    for i in range(len(string)):
        if string[i] != '/':
            continue
        elif string[i] == '/':
            j = i + 1
            for i in range(j, len(string)):
                tmp.append(string[i])
        break
    word = "".join(tmp)
    return word


def extractno(string):
    number = []
    for word in string:
        if word.isdigit():
            number.append(word)
    number2 = "".join(number)
    return number2

def checkLoading(dri):
    state = dri.execute_script('return document.readyState')
    while state != "complete":
        time.sleep(1)
        state = dri.execute_script('return document.readyState')
    return

# horse info scrap
def scrap_horse(path, local_save_path):
    browser = webdriver.Chrome()
    horseLinkFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("horselink.txt")
    fo = open(horseLinkFilePath, 'r')
    try:
        local_save_path
    except NameError:
        local_save_path = None
    # Local Horse Records
    if local_save_path is not None:
        lfo2 = open(local_save_path + r"\h_trackwork.txt".replace("\u202a", ""), 'a')
        lfo3 = open(local_save_path + r"\h_veterinary.txt".replace("\u202a", ""), 'a')
        lfo4 = open(local_save_path + r"\h_cant.txt".replace("\u202a", ""), 'a')

    # Clear File
    horseCsvFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("horse.csv")
    trackworkFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("trackwork.txt")
    veterinaryFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("veterinary.txt")
    cantFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("cant.txt")
    horselinkscraFilePath = path + generatedFileUtil.generateFileNameWithCurrentDate("horselinkscra.txt")
    open(horseCsvFilePath, 'w').close()
    open(trackworkFilePath, 'w').close()
    open(veterinaryFilePath, 'w').close()
    open(cantFilePath, 'w').close()
    open(horselinkscraFilePath, 'w').close()

    # Write again
    horsefile = open(path + r'\horse.csv'.replace("\u202a", ""), 'a')
    horsewriter = csv.writer(horsefile, lineterminator='\n')
    horsewriter.writerow(['horse_id', 'name', ' origin', 'age', 'colour', 'sex', 'import_type', 'sire',
                          'dam', 'dam_sire'])
    fo2 = open(trackworkFilePath, 'a')
    fo3 = open(veterinaryFilePath, 'a')
    fo4 = open(cantFilePath, 'a')
    fo5 = open(horselinkscraFilePath, 'a')

    # 2/10/2018 http://racing.hkjc.com/racing/information/English/Horse/Horse.aspx?HorseId=HK_2017_B456
    for line in fo:
        try:
            url = line.replace("\n", "")
            # initiate
            browser.get(url)
            checkLoading(browser)
            html = browser.page_source
            soup = BeautifulSoup(html, 'html.parser')
            # horse id
            tmp = soup.select(horse_id_and_name_selector)[0].text
            horse_id = re.findall(r"\(([A-Za-z0-9_]+)\)", tmp)[0]
            # name
            tmp = soup.select(horse_id_and_name_selector)[0].text
            tmp2 = []
            for char in tmp:
                if char == "(":
                    tmp2.pop(-1)
                    break
                tmp2.append(char)
            name = "".join(tmp2)
            # origin
            tmp = soup.select(origin_and_age_selector)[0].text
            origin = extractword(tmp)
            origin = "".join(re.findall("[a-zA-Z]", tmp))
            # age
            tmp = soup.select(origin_and_age_selector)[0].text
            age = "".join(re.findall("[\d]", tmp))
            # colour
            tmp = soup.select(colour_and_sex_selector)[0].text
            colour = extractword(tmp.replace(" ", ""))
            # sex
            tmp = soup.select(colour_and_sex_selector)[0].text
            sex = extractword2(tmp.replace(" ", ""))
            # import type
            tmp = soup.select(import_type_selector)[0].text
            imty = "".join(re.findall("[a-zA-Z]", tmp))
            # sire
            if not soup.select(sire_selector):
                tmp = soup.select(another_sire_selector)[0].text
            else:
                tmp = soup.select(sire_selector)[0].text
            sire = extractword(tmp)
            # dam
            if 'Dam' in soup.select(dam_name_selector)[0].text:
                tmp = soup.select(another_dam_selector)[0].text
            else:
                tmp = soup.select(dam_selector)[0].text
            dam = extractword(tmp)
            # dam sire
            if not soup.select(dam_sire_selector):
                tmp = soup.select(another_dam_sire_selector)[0].text
            else:
                tmp = soup.select(dam_sire_selector)[0].text
            dmsi = extractword(tmp)
            # Trackwork Records
            if soup.select(trackwork_records_selector):
                tmp = soup.select(trackwork_records_selector)[0]['href']
                trwk = "http://racing.hkjc.com" + tmp + "\n"
                fo2.write(trwk)
                if local_save_path is not None:
                    lfo2.write(trwk)
            # Veterinary Records
            if soup.select(veterinary_records_selector):
                tmp = soup.select(veterinary_records_selector)[0]['href']
                very = "http://racing.hkjc.com" + tmp + "\n"
                fo3.write(very)
                if local_save_path is not None:
                    lfo3.write(very)
            # output csv
            horsewriter.writerow([horse_id, name, origin, age, colour, sex, imty, sire,
                                  dam, dmsi])
            # scraped link
            fo5.write(url + "\n")
        except Exception as e:
            fo4.write(url + "\n")
            print("Warning..." + str(e) + " " + url)
            if local_save_path is not None:
                lfo4.write(url + "\n")
            continue

    browser.close()
    horsefile.close()
    fo.close()
    fo2.close()
    fo3.close()
    fo4.close()
    if local_save_path is not None:
        lfo2.close()
        lfo3.close()
        lfo4.close()
    fo5.close()
