import os
import time

from src.config.horseRaceDataDbConnection import connection
from src.utils import calculatedFieldUtil, setsForModel, nullDataUtil, tensorModelUtil, sortResultUtil, \
    predictResultUtil

cur = connection.cursor()
DIR_PATH = os.path.dirname(os.path.realpath(__file__))

PREDIC_RACE_SCHEMA = 'predic_race'
RACE_ID_TO_PREDICT = '2007042507'
MODEL_PATH = DIR_PATH + r'\config\selectedModel'
metaPath = MODEL_PATH + r'\epoch20.ckpt.meta'
checkPointPath = MODEL_PATH + r'\epoch20.ckpt'
predictionSetTable = rf"{PREDIC_RACE_SCHEMA}.records_{RACE_ID_TO_PREDICT}"


def exportRaceToNewTable(raceId):
    cur.execute(rf'''DROP TABLE IF EXISTS {predictionSetTable};''')
    cur.execute(rf'''create table IF NOT EXISTS {predictionSetTable} as 
        select * from records where race_id = '{raceId}'; ''')


def calculateFieldForPredictionSet(tableToUpdate):
    calculatedFieldUtil.dayApartUpdate(0, tableToUpdate)
    calculatedFieldUtil.hPastPlaceUpdate(0, tableToUpdate)
    # must remove invalid r_place now
    calculatedFieldUtil.removeInvalidRPlace(None, tableToUpdate)
    calculatedFieldUtil.rWgtDiffUpdate(0, tableToUpdate)
    calculatedFieldUtil.horseEloUpdate(0, tableToUpdate)
    calculatedFieldUtil.jockeyEloUpdate(0, tableToUpdate)
    calculatedFieldUtil.trainerEloUpdate(0, tableToUpdate)
    calculatedFieldUtil.horseWinPercent(0, tableToUpdate)
    calculatedFieldUtil.jockeyWinPercent(0, tableToUpdate)
    calculatedFieldUtil.trainerWinPercent(0, tableToUpdate)
    nullDataUtil.hDayApartUpdate(tableToUpdate)
    nullDataUtil.rWgtDiffUpdate(tableToUpdate)
    nullDataUtil.hPastPlaceUpdate(tableToUpdate)


def predictSelectedRaceId():
    time_start = time.time()
    exportRaceToNewTable(RACE_ID_TO_PREDICT)
    calculateFieldForPredictionSet(predictionSetTable)
    sess, savedGraph = tensorModelUtil.loadSavedModel(metaPath, checkPointPath)
    predicSetDf = setsForModel.loadPredictionSetOrderByDraw(predictionSetTable)
    tensorModelUtil.feedToModelAndPredict(predictionSetTable, sess, savedGraph, predicSetDf, checkPointPath)
    time_stop = time.time()
    print(f'Time Used: {time_stop - time_start}')


# predictSelectedRaceId()
