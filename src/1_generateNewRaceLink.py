import os
import requests
from bs4 import BeautifulSoup

from src.config.horseRaceDataDbConnection import connection
from src.utils import generatedFileUtil

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
fileName = generatedFileUtil.generateFileNameWithCurrentDate("newRaceLink.txt")
newRaceLinkFiles = DIR_PATH + rf"\generatedFiles\1_generateNewRaceLink{fileName}"

cur = connection.cursor()

# Delete data used to predict
cur.execute('''delete from records where r_place is null;''')
cur.execute('''delete from race using 
(select race.race_id ra,records.race_id re from race left join records on race.race_id = records.race_id
 where records.race_id is null) t1
where t1.ra = race.race_id and t1.re is null;''')

cur.execute('''select race_id from records order by race_id desc limit 1''')

rows = cur.fetchall()

print('Current Version: ', rows[0][0])

cur.execute('''select r_date from race where race_id = ''' + rows[0][0] + "::text")

rows = cur.fetchall()
date = str(rows[0][0])

# Beautiful Soup Time

url = "http://www.hkhorsedb.com/cseh/ctop.php"
response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')
soup2 = soup.select("select option")
soup3 = []
for item in soup2:
    soup3.append(item.text)

# last:http://racing.hkjc.com/racing/info/meeting/Results/chinese/Local/20061223/ST

soup4 = []
for item in soup3:
    number = []
    for word in item:
        if word.isdigit():
            number.append(word)
    if "".join(number) == "":
        continue
    elif "".join(number) != date.replace("-", ""):
        soup4.append("".join(number))
    else:
        break

if soup4:  # soup4 is not empty means current database is not updated
    print("Generating new website to newRacelink.txt...")
    soup5 = []
    for item in soup4:
        soup5.insert(0, "http://racing.hkjc.com/racing/info/meeting/Results/English/Local/" + item)

    fo = open(newRaceLinkFiles.replace("\u202a", ""), "w")
    for item in soup5:
        fo.write(item + "\n")
    print("Done!")
    fo.close()

else:
    print("Current database is the latest version")
