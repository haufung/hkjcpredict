import os
import sys
from src.config.horseRaceDataDbConnection import connection
from src.utils import sqlQueryUtil

cur = connection.cursor()

root_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(root_path)

sql_path = root_path + r"\config\balanceLabel"


def executeSqlQueryFromTxt(path):
    fo = open(path, 'r')
    sqlFile = fo.read()
    fo.close()
    ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
    cur.execute(ln_sql)


sqlQueryUtil.executeSqlQueryFromTxt(sql_path + r"\duplicateRowFunction")

cur.execute(rf'''create table IF NOT EXISTS records_with_balanced_label as select * from records;''')
cur.execute('''select distinct race_id, r_hr_num from records_with_balanced_label''')
row = cur.fetchall()
for (raceId, horseNum) in row:
    loopTime = int(horseNum) - 2
    for i in range(loopTime):
        cur.execute(rf'''select "balance_label_for_this_race_id"('{raceId}');''')
    print(raceId)

connection.commit()
print("Complete")
