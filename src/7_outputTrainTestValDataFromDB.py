import os
from src.config.horseRaceDataDbConnection import connection
from src.utils import generatedFileUtil, setsForModel

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

cur = connection.cursor()

generatedFileDir = DIR_PATH + r"\generatedFiles\7_outputTrainTestValDataFromDB"
recordsTableNameForTraining = 'records_with_balanced_label'
recordsTableNameForTestingAndVal = 'records'

validationSetFilePath = generatedFileDir + generatedFileUtil.generateFileNameWithCurrentDate("validationSet.csv")
testingSetFilePath = generatedFileDir + generatedFileUtil.generateFileNameWithCurrentDate("testingSet.csv")
trainingSetFilePath = generatedFileDir + generatedFileUtil.generateFileNameWithCurrentDate("trainingSet.csv")

setsForModel.outPutValidationSet(recordsTableNameForTestingAndVal, validationSetFilePath)
setsForModel.outPutTestingSet(recordsTableNameForTestingAndVal, testingSetFilePath)
setsForModel.outPutTrainingSet(recordsTableNameForTraining, trainingSetFilePath)

