import csv
import os
import sys

import psycopg2

import horsedata

root_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(root_path)

update_path = root_path + r"\update"
sql_path = root_path + r"\Extra Feature\used"
del_path = root_path + r"\Extra Feature\used\deleted_records.csv"
local_save_path = root_path + r"\update\local_save"

with open('credential') as f:
    credentials = [x.strip().split(':', 5) for x in f]

for database, username, password, host, port, cloud_backup_path in credentials:
    conn = psycopg2.connect(database=database, user=username, password=password, host=host, port=port)

print("Opened database successfully")
cur = conn.cursor()

cur.execute(r'''begin;''')

# records the latest date
cur.execute(r'''select race_id from records order by race_id desc limit 1;''')
rows = cur.fetchall()
date = rows[0][0]
# 2018-06-10 '2018061011'

# import race.csv
cur.execute(r'''COPY race(race_id,r_date,r_loc,r_num,r_class,distance,going,course,pool) 
FROM '%s\race.csv' DELIMITER ',' CSV HEADER;''' % update_path)

print("imported race.csv")

# import records.csv
cur.execute(r'''COPY update(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,run_pos,fin_time,win_odds,h_day_apart,h_past_place,r_wgt_diff,r_h_winrate,r_h_total,r_j_total,r_j_winrate,r_t_total,r_t_winrate,after_h_elo,before_h_elo,after_j_elo,before_j_elo,after_t_elo,before_t_elo,label) 
FROM '%s\records.csv' DELIMITER ',' CSV HEADER;''' % update_path)

print("imported records.csv")

cur.execute(r'''select distinct horse_id from update 
  where (horse_id) not in
   ( select horse_id from horse )''')

rows = cur.fetchall()
cur.execute(r'''rollback;''')
nw_horse = []
for item in rows:
    nw_horse.append(item[0])

if not nw_horse:
    print("No new horse found")
else:
    print("New horse found:")
    for horse in nw_horse:
        print(horse)
    print("Scraping new horse data...")
    horsedata.gen_link(nw_horse, update_path, local_save_path)
    horsedata.scrap_horse(update_path, local_save_path)
    # import horse.csv
    cur.execute(r'''COPY horse(horse_id,name,origin,age,colour,sex,import_type,sire,dam,dam_sire) 
    FROM '%s\nw-horse\horse.csv' DELIMITER ',' CSV HEADER;''' % update_path)
    print("imported horse.csv")

# import race.csv
cur.execute(r'''COPY race(race_id,r_date,r_loc,r_num,r_class,distance,going,course,pool) 
FROM '%s\race.csv' DELIMITER ',' CSV HEADER;''' % update_path)

print("imported race.csv")

# import records.csv
cur.execute(r'''COPY records(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,run_pos,fin_time,win_odds,h_day_apart,h_past_place,r_wgt_diff,r_h_winrate,r_h_total,r_j_total,r_j_winrate,r_t_total,r_t_winrate,after_h_elo,before_h_elo,after_j_elo,before_j_elo,after_t_elo,before_t_elo,label) 
FROM '%s\records.csv' DELIMITER ',' CSV HEADER;''' % update_path)

print("imported records.csv")

# day_apart_func // Follow sql_path
fo = open(sql_path + r"\day_apart_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# day_apart update
cur.execute('''update records 
set h_day_apart = alias.day_apart
from (select  "day_apart"(records.horse_id, records.race_id) as day_apart, horse_id, r_date, race.race_id from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias
where records.race_id = alias.race_id and records.horse_id = alias.horse_id;'''.replace("\n", " ").replace("\t", " ") % (date))
print("day_apart update completed!")

# past_place_func
fo = open(sql_path + r"\past_place_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# past_place update
cur.execute('''update records 
set h_past_place = alias.past_place
from (select  "past_place"(records.horse_id, race.race_id, race.course) as past_place, records.race_id, records.horse_id, race.course from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id and race.course = alias.course ;'''.replace(
    "\n", " ").replace("\t", " ") % (date))
print("past_place update completed!")

# save all delete row // Follow del_path
cur.execute('''Copy (select * from records where
r_place = 'WV'
or r_place = 'WV-A'
or r_place ='WX'
or r_place ='PU'
or r_place ='UR'
or r_place ='WX-A'
or r_place ='FE'
or r_place ='DNF'
or r_place ='TNP'
or r_place ='DISQ'
or r_place ='WXNR'
or r_place is null) to '%s' with csv'''.replace("\n", " ").replace("\t", " ") % (del_path))
print("Saved all useless records")

# save to local file // Follow local_save_path
del_file = open(del_path, 'r')
del_file_reader = csv.reader(del_file, delimiter=' ', quotechar='|')
local_del_file = open(local_save_path + r"\deleted_records.csv", 'a', newline='')
local_writer = csv.writer(local_del_file, delimiter=' ', quotechar='|')

for row in del_file_reader:
    local_writer.writerow(row)

local_del_file.close()
del_file.close()
print("Saved all useless records to local files")

# delete useless row
fo = open(sql_path + r"\delete_records_sql.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)
print("Deleted useless records")

# wgt_diff_func
fo = open(sql_path + r"\wgt_diff_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# wgt_diff update
cur.execute('''update records 
set r_wgt_diff  = alias.wgt_diff
from (select  "r_wgt_diff"(records.horse_id, race.race_id, records.decalr_wt) as wgt_diff, records.race_id, records.horse_id, records.decalr_wt from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id and records.decalr_wt = alias.decalr_wt;'''.replace("\n", " ").replace("\t", " ") % (date))
print("wgt_diff update completed!")

# horse_elo_func
fo = open(sql_path + r"\horse_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# horse_elo update
cur.execute(
    '''select "compute_elo_k"(2250,alias.race_id)
     from (select distinct race_id from records 
     where race_id > '%s' order by race_id) as alias;'''.replace("\n", " ").replace("\t", " ") % (date))
print("horse_elo update completed!")

# jockey_elo_func
fo = open(sql_path + r"\jockey_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# jockey_elo update
cur.execute(
    '''select "compute_elo_k"(50,alias.race_id) 
    from (select distinct race_id from records 
    where race_id > '%s' order by race_id) as alias;'''.replace("\n", " ").replace("\t", " ") % (date))
print("jockey_elo update completed!")

# trainer_elo_func
fo = open(sql_path + r"\trainer_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# trainer_elo update
cur.execute(
    '''select "compute_elo_k"(1000,alias.race_id) 
    from (select distinct race_id from records 
    where race_id > '%s' order by race_id) as alias;'''.replace("\n", " ").replace("\t", " ") % (date))
print("trainer_elo update completed!")

# win_percent_horse_func
fo = open(sql_path + r"\win_percent_horse_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_horse update
cur.execute('''update records 
set r_h_winrate  = alias.r_h_winrate
from (select  "r_h_winrate"(records.horse_id, race.race_id) as r_h_winrate, records.race_id, records.horse_id from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
print("win_percent_horse update completed!")

# win_percent_jockey_func
fo = open(sql_path + r"\win_percent_jockey_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_jockey update
cur.execute('''update records 
set r_j_winrate  = alias.r_j_winrate
from (select  "r_j_winrate"(records.rjockey, records.race_id) as r_j_winrate, race_id, rjockey from records 
where records.race_id > '%s') as alias 
where records.rjockey = alias.rjockey 
and records.race_id = alias.race_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
print("win_percent_jockey update completed!")

# win_percent_trainer_func
fo = open(sql_path + r"\win_percent_trainer_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_trainer update
cur.execute('''update records 
set r_t_winrate  = alias.r_t_winrate
from (select  "r_t_winrate"(records.trainer, records.race_id) as r_t_winrate, race_id, trainer from records 
where records.race_id > '%s') as alias 
where records.trainer = alias.trainer
and records.race_id = alias.race_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
print("win_percent_trainer update completed!")

# cur.execute(r'''rollback;''')
# '2018061011'
cur.execute('''select race_id 
            from records order by race_id desc limit 1''')
rows = cur.fetchall()
updated = rows[0][0]
print("All Feature Extracted! Updated Until:", updated)
