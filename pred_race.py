import csv

path = r'''C:\Users\HauFung\OneDrive - The Chinese University of Hong Kong\Project\hkjockey\hkjcpredict\predict'''

# open record.csv with headings
open(path + r"\records.csv".replace("\u202a", ""), 'w').close()  # clear the previous record data
recordfile = open(path + r"\records.csv".replace("\u202a", ""), 'a')
recordwriter = csv.writer(recordfile, lineterminator='\n')

recordwriter.writerow(['race_id', 'r_place', 'r_hr_num', 'horse_id', 'rjockey', 'trainer',
                       'act_wt', 'decalr_wt', 'draw', 'lbw', 'run_pos', 'fin_time', 'win_odds',
                       'h_day_apart', 'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_h_total',
                       'r_j_total', 'r_j_winrate', 'r_t_total', 'r_t_winrate', 'after_h_elo',
                       'before_h_elo', 'after_j_elo', 'before_j_elo', 'after_t_elo', 'before_t_elo',
                       'label'])

# open race.csv with headings
open(path + r"\race.csv".replace("\u202a", ""), 'w').close()  # clear the previous race data
racefile = open(path + r"\race.csv".replace("\u202a", ""), 'a')
racewriter = csv.writer(racefile, lineterminator='\n')
racewriter.writerow(['race_id', 'r_date', 'r_loc', 'r_num', 'r_class', 'distance',
                     'going', 'course', 'pool'])

from bs4 import BeautifulSoup
import datetime
import re

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

browser = webdriver.Chrome()
url = "http://racing.hkjc.com/racing/Info/meeting/RaceCard/english/Local/"
browser.get(url)
wait = WebDriverWait(browser, 100)
element = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'tr[valign="middle"]')))
html = browser.page_source
soup = BeautifulSoup(html, 'html.parser')

# num of race
num = len(soup.select('tr[valign="middle"]')[0].select('td[nowrap="nowrap"] img'))
# Generate links to be scrap
link = []
s = soup.select("table.font13.lineH20.tdAlignL td br")[0].next_sibling
date = re.findall("(\w+ \d+\, \d+\,)", s)  # ['October 07, 2018,']
date1 = datetime.datetime.strptime(date[0], '%B %d, %Y,')  # datetime.datetime(2018, 10, 7, 0, 0)
date2 = date1.strftime('%Y%m%d')  # '20181007'
# Location//Race
if "Sha Tin" in s:
    r_loc = "ST"
elif "Happy Valley" in s:
    r_loc = "HV"
else:
    print("Error in location")
    print(s)
for i in range(num):
    tmp = url + date2 + "/" + r_loc + "/" + str(i + 1)
    link.append(tmp)
print("link generated")
print(link)

# Date//Race
r_date = date1.date()

r_num = 0  # initiate r_num
for curr in link:
    browser.get(curr)
    wait = WebDriverWait(browser, 100)
    element = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'tr[valign="middle"]')))
    html = browser.page_source
    soup = BeautifulSoup(html, 'html.parser')

    # r_num(race number)//Race
    r_num += 1

    # race_id//Race + Record
    tmp = list(date2)
    if len(str(r_num)) == 1:  # race_no 0-9
        tmp.append("0" + str(r_num))
    elif len(str(r_num)) > 1:  # race_no > 9
        tmp.append(str(r_num))

    race_id = "".join(tmp)

    # r_class //Race
    tmp = list(soup.select("table.font13.lineH20.tdAlignL td br")[2].next_sibling)
    tmp2 = []
    i = -1
    while True:
        if tmp[i] != "\n":
            i -= 1
            continue
        else:
            i -= 1
            while True:
                if tmp[i] != ',':
                    tmp2.append(tmp[i])
                    i -= 1
                else:
                    break
            break

    tmp2 = "".join(tmp2)
    r_class = tmp2[::-1][1:]  # reverse the string and remove the first ' '

    # distance //Race
    tmp = list(soup.select("table.font13.lineH20.tdAlignL td br")[1].next_sibling)
    tmp2 = []
    i = -1
    while True:
        if tmp[i] != "M":
            i -= 1
            continue
        else:
            i -= 1
            while True:
                if tmp[i] != ' ':
                    tmp2.append(tmp[i])
                    i -= 1
                else:
                    break
            break
    tmp2 = "".join(tmp2)
    distance = tmp2[::-1]

    # going //Race
    tmp = list(soup.select("table.font13.lineH20.tdAlignL td br")[1].next_sibling)
    tmp2 = []
    i = -1
    while True:
        if tmp[i] != ' ':
            tmp2.append(tmp[i])
            i -= 1
        else:
            break
    tmp2 = "".join(tmp2)
    lgoing = tmp2[::-1]
    going = lgoing.upper()  # lower to upper case

    # course //Race
    tmp = soup.select("table.font13.lineH20.tdAlignL td br")[1].next_sibling.replace(str(lgoing), "").replace(
        str(distance) + "M", "")
    tmp2 = tmp.replace('''"''', "").replace(",", " -").upper()
    course = []
    i = 0
    while True:
        if tmp2[i].isupper() == False:
            i += 1
            continue
        else:
            while True:
                if tmp2[i:].isupper() == True:
                    course.append(tmp2[i])
                    i += 1
                else:
                    break
        break
    course = "".join(course)

    # pool(Prize Money) //Race
    tmp = soup.select("table.font13.lineH20.tdAlignL td br")[2].next_sibling.replace(",", "")
    tmp2 = []
    i = 0
    while True:
        if tmp[i] != '$':
            i += 1
            continue
        else:
            i += 1
            while True:
                if tmp[i] != ' ':
                    tmp2.append(tmp[i])
                    i += 1
                else:
                    break
        break
    pool = "".join(tmp2)

    # r_hr_num (Number of horses) //Records
    r_hr_num = len(soup.select("table.draggable.hiddenable tr.font13.tdAlignC"))

    # horse_id //Records
    horse_id = []
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[4]
        horse_id.append(tmp.text)

    # rjockey //Records
    rjockey = []
    regex = re.compile('[^a-zA-Z +]')  # Only Keep Space and Letters
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[6]
        rjockey.append(regex.sub('', tmp.text))

    # trainer //Records
    trainer = []
    regex = re.compile('[^a-zA-Z +]')  # Only Keep Space and Letters
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[9]
        trainer.append(regex.sub('', tmp.text))

    # act_wt(Carried Weight)//Records
    act_wt = []
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[5]
        act_wt.append(tmp.text)

    # decalr_wt (Declared Weight)//Records
    decalr_wt = []
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[12]
        decalr_wt.append(tmp.text)

    # draw//Records
    draw = []
    for i in range(r_hr_num):
        tmp = soup.select("table.draggable.hiddenable tr.font13.tdAlignC")[i].select("td")[8]
        draw.append(tmp.text)

    # Below in Records ="NULL" NOT FOR PREDICTION
    r_place = [''] * r_hr_num
    lbw = [''] * r_hr_num
    run_pos = [''] * r_hr_num
    fin_time = [''] * r_hr_num
    win_odds = [''] * r_hr_num

    # output record.csv
    for i in range(r_hr_num):
        recordwriter.writerow([race_id, r_place[i], r_hr_num, horse_id[i], rjockey[i], trainer[i],
                               act_wt[i], decalr_wt[i], draw[i], lbw[i], run_pos[i], fin_time[i], win_odds[i]])
    # output race.csv
    racewriter.writerow([race_id, r_date, r_loc, r_num,
                         r_class, distance, going, course, pool])

recordfile.close()
racefile.close()

# put to database calculate and delete it back
import psycopg2
import sys
import time

root_path = r"D:\Projects\hkjockey\self-update-pyxpostgre"
sys.path.append(root_path)
import horsedata

path = r"D:\Projects\hkjockey\self-update-pyxpostgre\predict"
sql_path = r"D:\Projects\hkjockey\self-update-pyxpostgre\Extra Feature\used"
local_save_path = r"D:\Projects\hkjockey\self-update-pyxpostgre\update\local_save"
pred_path = path + r"\pred_race.csv"

conn = psycopg2.connect(database="local-google", user="postgres", password="1234", host="localhost", port="5432")

print("Opened database successfully")
cur = conn.cursor()

# records the latest date
cur.execute(r'''select race_id from records order by race_id desc limit 1;''')
rows = cur.fetchall()
date = rows[0][0]

print("start updating records after %s ..." % date)
progress = 1
total = 17
time_start = time.time()  # start counting time

cur.execute(r'''begin;''')  # Later on rollback

# import records.csv to update
cur.execute(r'''delete from update''')  # clear it first
cur.execute(r'''COPY update(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,run_pos,fin_time,win_odds) 
FROM '%s\records.csv' DELIMITER ',' CSV HEADER;''' % path)
progress += 1
print("imported records.csv to update table", str(progress) + "/%s" % total)

cur.execute(r'''select distinct horse_id from update 
  where (horse_id) not in
   ( select horse_id from horse )''')

print("Finding new horse...")
rows = cur.fetchall()

if not rows:
    progress += 1
    print("No new horse found", str(progress) + "/%s" % total)
else:
    nw_horse = list(rows[0])
    progress += 1
    print("New horse found:")
    for horse in nw_horse:
        print(horse)
    print("Scraping new horse data...")
    horsedata.gen_link(nw_horse, path, local_save_path)
    horsedata.scrap_horse(path, local_save_path)
    # import horse.csv
    cur.execute(r'''COPY horse(horse_id,name,origin,age,colour,sex,import_type,sire,dam,dam_sire) 
    FROM '%s\nw-horse\horse.csv' DELIMITER ',' CSV HEADER;''' % path)
    print("imported new horse.csv file to horse table", str(progress) + "/%s" % total)

print("Updated New Horse... Now begin calculating")

cur.execute(r'''begin;''')

print("begins!")

# import race.csv
cur.execute(r'''COPY race(race_id,r_date,r_loc,r_num,r_class,distance,going,course,pool) 
FROM '%s\race.csv' DELIMITER ',' CSV HEADER;''' % path)
progress += 1
print("imported race.csv to race table", str(progress) + "/%s" % total)

# import records.csv
cur.execute(r'''COPY records(race_id,r_place,r_hr_num,horse_id,rjockey,trainer,act_wt,decalr_wt,draw,lbw,run_pos,fin_time,win_odds) 
FROM '%s\records.csv' DELIMITER ',' CSV HEADER;''' % path)
progress += 1
print("imported records.csv to records table", str(progress) + "/%s" % total)

# day_apart_func // Follow sql_path
fo = open(sql_path + r"\day_apart_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# day_apart update
cur.execute('''update records 
set h_day_apart = alias.day_apart
from (select  "day_apart"(records.horse_id, records.race_id) as day_apart, horse_id, r_date, race.race_id from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias
where records.race_id = alias.race_id and records.horse_id = alias.horse_id;'''.replace("\n", " ").replace("\t",
                                                                                                           " ") % (
                date))
progress += 1
print("day_apart update completed", str(progress) + "/%s" % total)

# past_place_func
fo = open(sql_path + r"\past_place_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# past_place update
cur.execute('''update records 
set h_past_place = alias.past_place
from (select  "past_place"(records.horse_id, race.race_id, race.course) as past_place, records.race_id, records.horse_id, race.course from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id and race.course = alias.course ;
update records
set h_past_place = 'NULL'
where h_past_place is null;'''.replace("\n", " ").replace("\t", " ") % (date))
progress += 1
print("past_place update completed", str(progress) + "/%s" % total)

# save all delete row // Follow del_path
progress += 1
print("Saved all useless records(Skip)", str(progress) + "/%s" % total)

# save to local file // Folow local_save_path
progress += 1
print("Saved all useless records to local files(Skip)", str(progress) + "/%s" % total)

# delete the row // Follow path
fo = open(path + r"\delete_records_sql.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)
progress += 1
print("deleted unwanted rows", str(progress) + "/%s" % total)

# wgt_diff_func
fo = open(sql_path + r"\wgt_diff_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# wgt_diff update
cur.execute('''update records 
set r_wgt_diff  = alias.wgt_diff
from (select  "r_wgt_diff"(records.horse_id, race.race_id, records.decalr_wt) as wgt_diff, records.race_id, records.horse_id, records.decalr_wt from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id and records.decalr_wt = alias.decalr_wt;'''.replace(
    "\n", " ").replace("\t", " ") % (date))
progress += 1
print("wgt_diff update completed", str(progress) + "/%s" % total)

# horse_elo_func
fo = open(sql_path + r"\horse_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# horse_elo update
cur.execute(
    '''select "compute_elo_k"(2250,alias.race_id) from (select distinct race_id from records where race_id > '%s' order by race_id) as alias;'''.replace(
        "\n", " ").replace("\t", " ") % (date))
progress += 1
print("horse_elo update completed", str(progress) + "/%s" % total)

# jockey_elo_func
fo = open(sql_path + r"\jockey_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# jockey_elo update
cur.execute(
    '''select "compute_elo_k"(50,alias.race_id) from (select distinct race_id from records where race_id > '%s' order by race_id) as alias;'''.replace(
        "\n", " ").replace("\t", " ") % (date))
progress += 1
print("jockey_elo update completed", str(progress) + "/%s" % total)

# trainer_elo_func
fo = open(sql_path + r"\trainer_elo_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# trainer_elo update
cur.execute(
    '''select "compute_elo_k"(1000,alias.race_id) from (select distinct race_id from records where race_id > '%s' order by race_id) as alias;'''.replace(
        "\n", " ").replace("\t", " ") % (date))
progress += 1
print("trainer_elo update completed", str(progress) + "/%s" % total)

# win_percent_horse_func
fo = open(sql_path + r"\win_percent_horse_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_horse update
cur.execute('''update records 
set r_h_winrate  = alias.r_h_winrate
from (select  "r_h_winrate"(records.horse_id, race.race_id) as r_h_winrate, records.race_id, records.horse_id from race, records 
where race.race_id = records.race_id and records.race_id > '%s') as alias , race
where records.race_id = alias.race_id and records.horse_id = alias.horse_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
progress += 1
print("win_percent_horse update completed", str(progress) + "/%s" % total)

# win_percent_jockey_func
fo = open(sql_path + r"\win_percent_jockey_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_jockey update
cur.execute('''update records 
set r_j_winrate  = alias.r_j_winrate
from (select  "r_j_winrate"(records.rjockey, records.race_id) as r_j_winrate, race_id, rjockey from records 
where records.race_id > '%s') as alias 
where records.rjockey = alias.rjockey 
and records.race_id = alias.race_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
progress += 1
print("win_percent_jockey update completed", str(progress) + "/%s" % total)

# win_percent_trainer_func
fo = open(sql_path + r"\win_percent_trainer_func.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)

# win_percent_trainer update
cur.execute('''update records 
set r_t_winrate  = alias.r_t_winrate
from (select  "r_t_winrate"(records.trainer, records.race_id) as r_t_winrate, race_id, trainer from records 
where records.race_id > '%s') as alias 
where records.trainer = alias.trainer
and records.race_id = alias.race_id;
'''.replace("\n", " ").replace("\t", " ") % (date))
progress += 1
print("win_percent_trainer update completed", str(progress) + "/%s" % total)

# correct null data
fo = open(sql_path + r"\missing_data_handle\update_all_sql.txt", 'r')
sqlFile = fo.read()
fo.close()
ln_sql = sqlFile.replace("\n", " ").replace("\t", " ")
cur.execute(ln_sql)
progress += 1
print("corrected null data", str(progress) + "/%s" % total)

# generate label
progress += 1
print("label generated(Skip)", str(progress) + "/%s" % total)

# cur.execute(r'''begin;''')
# cur.execute(r'''rollback;''')
# '2018061011'
cur.execute("select race_id from records order by race_id desc limit 1")
rows = cur.fetchall()
updated = rows[0][0]

time_stop = time.time()
duration = time_stop - time_start
print("Database updated! Updated Until:", updated, '|| %ss' % float('%.4g' % duration))

cur.execute(r'''commit;''')
cur.execute('''Copy (select * from records where race_id > '%s') to '%s' with csv header''' % (date, pred_path))
print("All calculated and backed up")

#########################################

root_path = r"D:\Projects\hkjockey\self-update-pyxpostgre"
sys.path.append(root_path)
import date_to_predict

conn = psycopg2.connect(database="local-google", user="postgres", password="1234", host="localhost", port="5432")

print("Opened database successfully")
cur = conn.cursor()
(result, hid, place) = date_to_predict.main(cur)

print("result generated!")

# DELETE PRED RACE
