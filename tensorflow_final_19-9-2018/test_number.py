import tensorflow as tf
import numpy as np
import time
import sys
sys.path.append(r"D:\Projects\hkjockey\py\tensorflow_final_19-9-2018")
import hkjc_data
import hkjc_feature_column2

number = 230

sess = tf.Session()
train_writer = tf.summary.FileWriter(r"D:\Projects\hkjockey\py\tensorflow_final_19-9-2018\train",
                                      sess.graph)
while number < 300:
    cp_path = r"D:\Projects\hkjockey\py\tensorflow_final_19-9-2018\model\model"+ str(number)+ ".ckpt"
    meta = r"D:\Projects\hkjockey\py\tensorflow_final_19-9-2018\model\model"+ str(number)+ ".ckpt.meta"
    my_feature_column = hkjc_feature_column2.get_feature_column(cp_path)
    print("Loading data...")
    (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = hkjc_data.load_data(my_feature_column)
    print("Completed!")

    saver = tf.train.import_meta_graph(meta)
    saver.restore(sess, cp_path)
    graph = tf.get_default_graph()

    x = graph.get_tensor_by_name("x:0")
    y = graph.get_tensor_by_name("y:0")


    win1 = graph.get_tensor_by_name("win1:0")

    num_race = len(val_test_batch)
    val_total_win = 0
    i = 0

    #Val_win
    for batch_size2 in val_test_batch:
            start = i
            end = i + batch_size2
            val_batch_x = np.array(val_test_x[start:end])
            val_batch_y = np.array(val_test_y[start:end])
            i+=batch_size2
            val_this_win = sess.run(win1, feed_dict={x: val_batch_x,
				                                    y: val_batch_y})
            val_total_win += val_this_win
   

    val_acc = val_total_win/num_race
    print('Val_win:',val_total_win, 'Out of:', num_race, 'Val_Acc: %s' % float('%.4g' % val_acc))

    num_race = len(test_batch)
    total_win = 0
    i = 0

    # Test_Win
    for batch_size1 in test_batch:
            start = i
            end = i + batch_size1
            batch_x = np.array(test_x[start:end])
            batch_y = np.array(test_y[start:end])
            i+=batch_size1
            this_win = sess.run(win1, feed_dict={x: batch_x,
				                                y: batch_y})
            total_win += this_win

    test_acc = total_win/num_race
    print('Test_win:',total_win, 'Out of:', num_race, 'Acc: %s' % float('%.4g' % test_acc))
    number+=10
    test_summary = tf.Summary(value=[tf.Summary.Value(tag="test_acc", simple_value=test_acc),tf.Summary.Value(tag="val_acc", simple_value=val_acc)])
    train_writer.add_summary(test_summary, (number))



