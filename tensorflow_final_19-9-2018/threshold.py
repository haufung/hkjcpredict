import csv

import numpy as np
import tensorflow as tf
import sys
sys.path.append('../')
import hkjc_data
import hkjc_feature_column

root_path = r"C:\Users\HauFung\OneDrive - The Chinese University of Hong Kong\Project\hkjockey\hkjcpredict\20200816_model"
csvfile = open(root_path + r"\all_win.csv", 'a', newline='')
spamwriter = csv.writer(csvfile)

cp_path = root_path + r"\modelmodel110.ckpt"
meta = root_path + r"\modelmodel110.ckpt.meta"
my_feature_column = hkjc_feature_column.get_feature_column()
print("Loading data...")
(train_x, train_y), (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = hkjc_data.load_data(my_feature_column)
print("Completed!")

sess = tf.Session()

saver = tf.train.import_meta_graph(meta)
saver.restore(sess, cp_path)
graph = tf.get_default_graph()
print("Completed!2")

x = graph.get_tensor_by_name("x:0")
y = graph.get_tensor_by_name("y:0")

pred1 = graph.get_tensor_by_name("pred:0")

win1 = graph.get_tensor_by_name("win1:0")

n_classes = 2
num_race = len(val_test_batch)
val_total_win = 0
i = 0
count = 0
# Val_win
for batch_size2 in val_test_batch:
    start = i
    end = i + batch_size2
    val_batch_x = np.array(val_test_x[start:end])
    val_batch_y = np.array(val_test_y[start:end])
    i += batch_size2

    # val_this_win , prediction = sess.run([win1,pred1], feed_dict={x: val_batch_x, y: val_batch_y})
    val_this_win = sess.run(win1, feed_dict={x: val_batch_x, y: val_batch_y})

    # if val_this_win == 1:
    prediction = sess.run(pred1, feed_dict={x: val_batch_x, y: val_batch_y})
    win_h = sess.run(tf.argmax(prediction, 0)[1])
    percentage = prediction[win_h][1]
    spamwriter.writerow([val_test_batch.index[count], percentage])
    val_total_win += val_this_win

    count += 1

val_acc = val_total_win / num_race
print('Val_win:', val_total_win, 'Out of:', num_race, 'Val_Acc: %s' % float('%.4g' % val_acc))

num_race = len(test_batch)
total_win = 0
i = 0
count = 0
# Test_Win
for batch_size1 in test_batch:
    start = i
    end = i + batch_size1
    batch_x = np.array(test_x[start:end])
    batch_y = np.array(test_y[start:end])
    i += batch_size1
    # this_win,prediction = sess.run([win1,pred1], feed_dict={x: batch_x,
    #		                            y: batch_y})
    this_win = sess.run(win1, feed_dict={x: batch_x, y: batch_y})
    # if this_win ==  1:
    prediction = sess.run(pred1, feed_dict={x: batch_x, y: batch_y})
    win_h = sess.run(tf.argmax(prediction, 0)[1])
    percentage = prediction[win_h][1]
    spamwriter.writerow([test_batch.index[count], percentage])
    total_win += this_win

    count += 1

test_acc = total_win / num_race
print('Test_win:', total_win, 'Out of:', num_race, 'Acc: %s' % float('%.4g' % test_acc))

csvfile.close()
