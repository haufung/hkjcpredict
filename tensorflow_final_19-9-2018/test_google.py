import tensorflow as tf
import numpy as np
import time
import hkjc_data
import hkjc_feature_column2


cp_path = r"/home/tanghaufung/tensorflow10-9-2018/model/model120.ckpt"
meta = r"/home/tanghaufung/tensorflow10-9-2018/model/model120.ckpt.meta"
my_feature_column = hkjc_feature_column2.get_feature_column(cp_path)
print("Loading data...")
(test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = hkjc_data.load_data(my_feature_column)
print("Completed!")


sess = tf.Session() 
sess.run((tf.global_variables_initializer(),tf.tables_initializer()))
saver = tf.train.import_meta_graph(meta)
saver.restore(sess, cp_path)
graph = tf.get_default_graph()

x = graph.get_tensor_by_name("x:0")
y = graph.get_tensor_by_name("y:0")

#pred0 = graph.get_tensor_by_name("pred0:0")
#pred1 = graph.get_tensor_by_name("pred1:0")

win1 = graph.get_tensor_by_name("win1:0")
#correct = tf.equal(tf.argmax(pred1, 0)[1], tf.argmax(y, 0)[1])
#win = tf.reduce_sum(tf.cast(correct, 'float'))
n_classes = 2
num_race = len(val_test_batch)
val_total_win = 0
i = 0

#Val_win
for batch_size2 in val_test_batch:
        start = i
        end = i + batch_size2
        val_batch_x = np.array(val_test_x[start:end])
        val_batch_y = np.array(val_test_y[start:end])
        i+=batch_size2
        val_this_win = sess.run(win1, feed_dict={x: val_batch_x,
				                                y: val_batch_y})
        val_total_win += val_this_win
   

val_acc = val_total_win/num_race
print('Val_win:',val_total_win, 'Out of:', num_race, 'Val_Acc: %s' % float('%.4g' % val_acc))

num_race = len(test_batch)
total_win = 0
i = 0

# Test_Win
for batch_size1 in test_batch:
        start = i
        end = i + batch_size1
        batch_x = np.array(test_x[start:end])
        batch_y = np.array(test_y[start:end])
        i+=batch_size1
        this_win = sess.run(win1, feed_dict={x: batch_x,
				                            y: batch_y})
        total_win += this_win

test_acc = total_win/num_race
print('Test_win:',total_win, 'Out of:', num_race, 'Acc: %s' % float('%.4g' % test_acc))



#from tensorflow.python.tools import inspect_checkpoint as chkp
#cp_path = r"D:\Projects\hkjockey\py\tensorflow_final_19-9-2018\model\model120.ckpt"

#chkp.print_tensors_in_checkpoint_file(cp_path, tensor_name='', all_tensors=True)


#sess.run(pred0, feed_dict={x: val_batch_x,
#				                            y: val_batch_y})