import tensorflow as tf
import numpy as np
import time
import sys
sys.path.append(r"D:\Projects\hkjockey\py\tensorflow19-9-2018_adj_lr_pred")
import hkjc_data
import hkjc_feature_column2


cp_path = r"‪D:\\Projects\\hkjockey\\py\\tensorflow10-9-2018222_new_val_test\\model\\model.ckpt160"
meta = r"‪D:\\Projects\\hkjockey\\py\\tensorflow10-9-2018222_new_val_test\\model\\model.ckpt160.meta"
my_feature_column = hkjc_feature_column2.get_feature_column(cp_path)
print("Loading data...")
_, (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = hkjc_data.load_data(my_feature_column)
print("Completed!")

n_classes = 2
x = tf.placeholder('float',shape=[None,153])
y = tf.placeholder('float',shape=[None,2])


# Nothing changes
def neural_network_model(net):

    for units in [100,50]:
        net = tf.layers.dense(net, units=units, activation=tf.nn.relu)
    logits = tf.layers.dense(net, units=n_classes , activation=None)

    return logits

def predict(x):
    sess=tf.Session()
    saver = tf.train.import_meta_graph(meta)
    saver.restore(sess, cp_path)
    print("Model restored.")
    # Add ops to save and restore all the variables.
    graph = tf.Graph()
    with graph.as_default():
    # [Variable and model creation goes here.]
        x = tf.placeholder('float',shape=[None,153])
        y = tf.placeholder('float',shape=[None,2])
        # my_feature_column = hkjc_feature_column.get_feature_column()
        prediction = neural_network_model(x)

        loss = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=y))

        optimizer = tf.train.AdamOptimizer(learning_rate=0.00075).minimize(loss)

    #sess.run((tf.global_variables_initializer(),tf.tables_initializer()))
    #sess = tf.Session(graph=graph)
    sess = tf.Session()
    saver = tf.train.import_meta_graph(meta)
    saver.restore(sess, cp_path)
    #print("Model restored.")



    #result = tf.nn.softmax(prediction,1)
    #result = tf.nn.softmax(prediction,0)
    #sess.run(result, feed_dict={x: val_batch_x})
    #result2 = tf.nn.softmax(result,1)
    #sess.run(result2, feed_dict={x: val_batch_x})

    correct = tf.equal(tf.argmax(tf.nn.softmax(prediction,1), 0)[1], tf.argmax(y, 0)[1])
    win = tf.reduce_sum(tf.cast(correct, 'float'))

    num_race = len(val_test_batch)
    val_total_win = 0
    i = 0
    #Val_win
    for batch_size2 in val_test_batch:
            start = i
            end = i + batch_size2
            val_batch_x = np.array(val_test_x[start:end])
            val_batch_y = np.array(val_test_y[start:end])
            i+=batch_size2
            break
            val_this_win = sess.run(win, feed_dict={x: val_batch_x,
				                                   y: val_batch_y})
            #print(sess.run('dense_2/bias', feed_dict={x: val_batch_x, y: val_batch_y}))
            val_total_win += val_this_win

    val_acc = val_total_win/num_race
    print('Val_win:',val_total_win, 'Out of:', num_race, 'Val_Acc: %s' % float('%.4g' % val_acc))

    num_race = len(test_batch)
    total_win = 0
    i = 0

    # Test_Win
    for batch_size1 in test_batch:
            start = i
            end = i + batch_size1
            batch_x = np.array(test_x[start:end])
            batch_y = np.array(test_y[start:end])
            i+=batch_size1
            this_win = sess.run(win, feed_dict={x: batch_x,
				                                y: batch_y})
            total_win += this_win

    test_acc = total_win/num_race
    print('Test_win:',total_win, 'Out of:', num_race, 'Acc: %s' % float('%.4g' % test_acc))

predict(x)




from tensorflow.python.tools import inspect_checkpoint as chkp
cp_path = r"D:\Projects\hkjockey\py\tensorflow19-9-2018_adj_lr_pred\model\feature_column.ckpt"

chkp.print_tensors_in_checkpoint_file(cp_path, tensor_name='', all_tensors=True)

