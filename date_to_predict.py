import os
import sys

import numpy as np
import pandas as pd
import psycopg2
import tensorflow as tf

dir_path = os.path.dirname(os.path.realpath(__file__))

RACE_COLUMN_NAMES = ['race_id', 'horse_id', 'r_place', 'r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt',
                     'draw', 'age', 'h_day_apart', 'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate',
                     'r_t_winrate', 'before_h_elo', 'before_j_elo', 'before_t_elo', 'label']
test_path = r"D:\Projects\hkjockey\data_for_tensorflow\used_data_10-9-2018\test.csv"
val_test_path = r"D:\Projects\hkjockey\data_for_tensorflow\used_data_10-9-2018\val_test.csv"

TEST_COLUMN_NAMES = ['race_id', 'r_loc', 'distance', 'going', 'course', 'act_wt', 'decalr_wt', 'draw', 'age',
                     'h_day_apart', 'h_past_place', 'r_wgt_diff', 'r_h_winrate', 'r_j_winrate', 'r_t_winrate',
                     'before_h_elo', 'before_j_elo', 'before_t_elo', 'label']

sys.path.append(
    r"C:\Users\HauFung\OneDrive - The Chinese University of Hong Kong\Project\hkjockey\py\tensorflow_final_19-9-2018")
import hkjc_feature_column2

conn = psycopg2.connect(database="local-google", user="postgres", password="1234", host="localhost", port="5432")

print("Opened database successfully")
cur = conn.cursor()


def load_data2(my_feature_column):
    test = pd.read_csv(test_path, names=TEST_COLUMN_NAMES, header=0,
                       converters={'race_id': str,
                                   'h_past_place': str,
                                   'distance': str,
                                   'age': str,
                                   'draw': str})

    test_batch = test.groupby("race_id").size()
    _ = test.pop('race_id')
    test_x, test_y = test, test.pop('label')
    test_y = label_to_class(test_y).values
    test_x = tf.feature_column.input_layer(dict(test_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        test_x = sess.run(test_x)

    val_test = pd.read_csv(val_test_path, names=TEST_COLUMN_NAMES, header=0,
                           converters={'race_id': str,
                                       'h_past_place': str,
                                       'distance': str,
                                       'age': str,
                                       'draw': str})

    val_test_batch = val_test.groupby("race_id").size()
    _ = val_test.pop('race_id')
    val_test_x, val_test_y = val_test, val_test.pop('label')
    val_test_y = label_to_class(val_test_y).values
    val_test_x = tf.feature_column.input_layer(dict(val_test_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        val_test_x = sess.run(val_test_x)

    return (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch)  # for testing


def label_to_class(series):
    series.loc[-1] = np.int64(1)
    series.loc[-2] = np.int64(0)
    series = pd.get_dummies(series)
    series = series.drop(series.index[[-1, -2]])

    return series


def load_data(my_feature_column, date, race_path, cur):
    cur.execute(r'''Copy (select records.race_id,horse.horse_id,r_place,r_loc, distance, going, course,act_wt, decalr_wt, draw, age, h_day_apart, h_past_place, r_wgt_diff, r_h_winrate, r_j_winrate, r_t_winrate, before_h_elo, before_j_elo, before_t_elo, label
    from records,race,horse 
    where race.race_id = records.race_id 
    and horse.horse_id = records.horse_id 
    and r_date = '%s' order by race_id asc) to '%s' with csv header'''.replace("\n", " ").replace("\t", " ") % (
        date, race_path))
    # =
    test = pd.read_csv(race_path, names=RACE_COLUMN_NAMES, header=0,
                       converters={'race_id': str,
                                   'horse_id': str,  # horse id
                                   'r_place': str,  # horse place *not for prediction
                                   'h_past_place': str,
                                   'distance': str,
                                   'age': str,
                                   'draw': str})

    test_batch = test.groupby("race_id").size()
    _, hid, r_place = test.pop('race_id'), test.pop('horse_id'), test.pop(
        'r_place')  # race_id, horse_id and r_place are useless
    test_x, test_y = test, test.pop('label')
    if np.isnan(test_y[0]) == False:
        test_y = label_to_class(test_y).values

    test_x = tf.feature_column.input_layer(dict(test_x), my_feature_column)
    with tf.Session() as sess:
        sess.run((tf.global_variables_initializer(), tf.tables_initializer()))
        test_x = sess.run(test_x)

    return (test_x, test_y, test_batch, hid, r_place)


def main(cur):
    # model_path = r'D:\Projects\hkjockey\self-update-pyxpostgre\model'4
    model_path = r'C:\Users\HauFung\OneDrive - The Chinese University of Hong Kong\Project\hkjockey\hkjcpredict\20200413_model'
    cp_path = model_path + r"\modelmodel110.ckpt"
    meta = model_path + r"\modelmodel110.ckpt.meta"
    race_path = r"D:\Projects\hkjockey\self-update-pyxpostgre\this_race.csv"
    my_feature_column = hkjc_feature_column2.get_feature_column(cp_path)
    print("Loading data...")

    cur.execute(r'''select r_date from race order by r_date desc limit 1''')  # select date of latest race in database
    rows = cur.fetchall()
    date = rows[0][0]
    # date = '2018-09-22'
    # date = '2018-06-10'
    # date = '2018-10-1'
    date = '2017-01-01'

    (test_x, test_y, test_batch, hid, r_place) = load_data(my_feature_column, date, race_path, cur)
    # (test_x, test_y, test_batch), (val_test_x, val_test_y, val_test_batch) = load_data(my_feature_column)

    print("Completed!")

    sess = tf.Session()

    saver = tf.train.import_meta_graph(meta)
    saver.restore(sess, cp_path)
    graph = tf.get_default_graph()

    x = graph.get_tensor_by_name("x:0")
    y = graph.get_tensor_by_name("y:0")

    # pred0 = graph.get_tensor_by_name("pred0:0")
    # pred1 = graph.get_tensor_by_name("pred1:0")
    pred = graph.get_tensor_by_name("pred:0")

    # win1 = graph.get_tensor_by_name("win1:0")
    # correct = tf.equal(tf.argmax(pred1, 0)[1], tf.argmax(y, 0)[1])
    # win = tf.reduce_sum(tf.cast(correct, 'float'))

    num_race = len(test_batch)
    result = []
    i = 0
    hid1 = []
    r_place1 = []
    # predict
    for batch_size1 in test_batch:
        start = i
        end = i + batch_size1
        batch_x = np.array(test_x[start:end])
        batch_hid, batch_r_place = hid[start:end], r_place[start:end]
        # batch_y = np.array(test_y[start:end])
        i += batch_size1
        this_result = sess.run(pred, feed_dict={x: batch_x})
        # y: batch_y})
        result.append(this_result)
        hid1.append(batch_hid)
        r_place1.append(batch_r_place)

    return (result, hid1, r_place1)


result, hid1, r_place1 = main(cur)

print("Result : " + str(result))
print("hid1 : " + str(hid1))
print("r_place1 : " + str(r_place1))
