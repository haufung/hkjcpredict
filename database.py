import psycopg2
import requests
from bs4 import BeautifulSoup
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
PATH = dir_path + r"\update\hkjclink.txt"
# Create Connect to PostgresSql
with open('credential') as f:
    credentials = [x.strip().split(':', 5) for x in f]

for database, username, password, host, port, cloud_backup_path in credentials:
    conn = psycopg2.connect(database=database, user=username, password=password, host=host, port=port)

print("Opened database successfully")
cur = conn.cursor()

# Delete data used to predict
cur.execute('''delete from records where r_place is null;''')
cur.execute('''delete from race using 
(select race.race_id ra,records.race_id re from race left join records on race.race_id = records.race_id
 where records.race_id is null) t1
where t1.ra = race.race_id and t1.re is null;''')

cur.execute('''select race_id from records order by race_id desc limit 1''')

rows = cur.fetchall()

print('Current Version: ', rows[0][0])

cur.execute('''select r_date from race where race_id = ''' + rows[0][0] + "::text")

rows = cur.fetchall()
date = str(rows[0][0])

# Beautiful Soup Time

url = "http://www.hkhorsedb.com/cseh/ctop.php"
response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')
soup2 = soup.select("select option")
soup3 = []
for item in soup2:
    soup3.append(item.text)

# last:http://racing.hkjc.com/racing/info/meeting/Results/chinese/Local/20061223/ST

soup4 = []
for item in soup3:
    number = []
    for word in item:
        if word.isdigit():
            number.append(word)
    if "".join(number) == "":
        continue
    elif "".join(number) != date.replace("-", ""):
        soup4.append("".join(number))
    else:
        break

if soup4:  # soup4 is not empty means current database is not updated
    print("Generating new website to hkjclink.txt...")
    soup5 = []
    for item in soup4:
        soup5.insert(0, "http://racing.hkjc.com/racing/info/meeting/Results/English/Local/" + item)

    fo = open(PATH.replace("\u202a", ""), "w")
    for item in soup5:
        fo.write(item + "\n")
    print("Done!")
    fo.close()

else:
    print("Current database is the latest version")
